package ${packageName}.mapper;

import java.util.List;

import ${packageName}.domain.${ClassName};

/**
 * ${functionName}Mapper接口
 *
 * @author ${author}
 * @date ${datetime}
 */
public interface ${ClassName}Mapper {
    /**
     * 查询${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    public ${ClassName} select${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField});


      /**
         * 批量查询${functionName}
         *
         * @param ${pkColumn.javaField}s 需要查询的数据ID
         * @return 结果
         */
      public List<${ClassName}>  select${ClassName}ByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s);

    /**
     * 查询${functionName}列表
     *
     * @param ${className} ${functionName}
     * @return ${functionName}集合
     */
    public List<${ClassName}> select${ClassName}List(${ClassName} ${className});

    /**
     * 新增${functionName}
     *
     * @param ${className} ${functionName}
     * @return 结果
     */
    public int insert${ClassName}(${ClassName} ${className});

    /**
     * 修改${functionName}
     *
     * @param ${className} ${functionName}
     * @return 结果
     */
    public int update${ClassName}(${ClassName} ${className});

    /**
     * 删除${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    public int delete${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField});

    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的数据ID
     * @return 结果
     */
    public int delete${ClassName}ByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s);
}
