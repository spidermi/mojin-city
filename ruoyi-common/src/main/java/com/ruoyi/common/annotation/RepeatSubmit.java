/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:RepeatSubmit.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解防止表单重复提交
 *
 * @author ruoyi
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {

}
