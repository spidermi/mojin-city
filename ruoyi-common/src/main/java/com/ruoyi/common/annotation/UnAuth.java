/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:UnAuth.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 不需要拦截注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UnAuth {
}
