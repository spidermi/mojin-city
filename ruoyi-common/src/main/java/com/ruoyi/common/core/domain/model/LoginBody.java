/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:LoginBody.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.core.domain.model;

import lombok.Data;

/**
 * 用户登录对象
 *
 * @author ruoyi
 */
@Data
public class LoginBody {
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 是否需要验证码 0 需要
     */
    private int needCode = 0;

    /**
     * 唯一标识
     */
    private String uuid = "";

}
