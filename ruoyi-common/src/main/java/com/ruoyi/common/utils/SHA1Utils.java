/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:SHA1Utils.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.utils;

import java.security.MessageDigest;

/**
 * SHA1加密工具类
 *
 * @author Caizize created on 2019/5/8
 */
public class SHA1Utils {

    private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
            'e', 'f'};

    private static String getFormattedText(byte[] bytes) {
        int len = bytes.length;
        StringBuilder buf = new StringBuilder(len * 2);
        // 把密文转换成十六进制的字符串形式
        for (int j = 0; j < len; j++) {
            buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
            buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
        }
        return buf.toString();
    }

    /**
     * 微信接入需要的SHA1加密
     *
     * @param str 加密之前的字符串
     * @return SHA1加密后的字符串
     */
    public static String encode(String str) {
        if (str == null) {
            return null;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            messageDigest.update(str.getBytes());
            return getFormattedText(messageDigest.digest());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            System.out.println(RandomMathLetter.randomInt(11));
        }
    }
}
