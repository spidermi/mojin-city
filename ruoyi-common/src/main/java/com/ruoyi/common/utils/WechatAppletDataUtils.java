/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WechatAppletDataUtils.java
 * Date:2021/01/09 11:40:09
 */

package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.utils.bean.WechatSetting;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WechatAppletDataUtils {


    /**
     * 获取用户访问小程序日留存
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/visit-retain/analysis.getDailyRetain.html
     *
     * @param wechatSetting 微信设置实体
     * @return 微信小程序access_token
     */
    public static String getDailyRetain(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappiddailyretaininfo?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序月留存
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/visit-retain/analysis.getDailyRetain.html
     *
     * @param wechatSetting 微信设置实体
     *                      开始日期，为自然月第一天。格式为 yyyymmdd
     *                      结束日期，为自然月最后一天，限定查询一个月数据。格式为 yyyymmdd
     *                      返回值
     * @return 微信小程序access_token
     */
    public static String getMonthlyRetain(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappidmonthlyretaininfo?access_token=%s";
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序周留存
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/data-analysis/visit-retain/analysis.getDailyRetain.html
     *
     * @param wechatSetting 微信设置实体
     *                      开始日期，为周一日期。格式为 yyyymmdd
     *                      结束日期，为周日日期，限定查询一周数据。格式为 yyyymmdd
     *                      返回值
     * @return 微信小程序access_token
     */
    public static String getWeeklyRetain(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappidweeklyretaininfo?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序数据概况
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getDailySummary(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappiddailysummarytrend?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序数据日趋势
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getDailyVisitTrend(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappiddailyvisittrend?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序数据月趋势(能查询到的最新数据为上一个自然月的数据)
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getMonthlyVisitTrend(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappidmonthlyvisittrend?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序数据周趋势
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getWeeklyVisitTrend(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappidweeklyvisittrend?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户访问小程序数据周趋势
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getPerformanceData(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/wxa/business/performance/boot?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }


    /**
     * 获取小程序新增或活跃用户的画像分布数据。时间范围支持昨天、最近7天、最近30天。其中，新增用户数为时间范围内首次访问小程序的去重用户数，活跃用户数为时间范围内访问过小程序的去重用户数
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getUserPortrait(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappiduserportrait?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 获取用户小程序访问分布数据
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getVisitDistribution(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappidvisitdistribution?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }

    /**
     * 访问页面。目前只提供按 page_visit_pv 排序的 top200。
     *
     * @param wechatSetting
     * @param param
     * @return
     */
    public static String getVisitPage(WechatSetting wechatSetting, WechatParam param) {
        // 设置获取微信小程序码接口调用凭证
        String accessToken = WeChatAppletUtils.getAccessToken(wechatSetting);
        String url = "https://api.weixin.qq.com/datacube/getweanalysisappidvisitpage?access_token=%s";
        // param.setBegin_date("20210106");param.setEnd_date("20210106");
        String json = CustomHttpUtils.postJson(String.format(url, accessToken), JSON.toJSONString(param));
        return json;
    }


    public static void main(String[] args) {
        WechatParam param = new WechatParam();
        WechatSetting wechatSetting = new WechatSetting();
        wechatSetting.setAppId("wx6007249d6b2348ea");
        wechatSetting.setAppSecret("1b1c5e732f38a03c980061855e68a19a");
        param.setBegin_date("20210107");
        param.setEnd_date("20210107");
        System.out.printf(getDailySummary(wechatSetting, param));
        //   System.out.printf(getDailyVisitTrend(wechatSetting,param));
        // System.out.printf(getUserPortrait(wechatSetting,param));
        //  System.out.printf(getVisitPage(wechatSetting,param));
        //  System.out.printf(getDailyRetain(wechatSetting,param));
        param.setBegin_date("20201201");
        param.setEnd_date("20201231");
        //  System.out.printf(getMonthlyRetain(wechatSetting,param));
        param.setBegin_date("20201228");
        param.setEnd_date("20210103");
        // System.out.printf(getWeeklyRetain(wechatSetting,param));
    }
}
