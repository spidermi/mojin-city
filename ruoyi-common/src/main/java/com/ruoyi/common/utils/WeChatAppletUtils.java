/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WeChatAppletUtils.java
 * Date:2021/01/09 11:40:09
 */

package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.ruoyi.common.config.WechatConfig;
import com.ruoyi.common.constant.KeysConstant;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.bean.WeChatAppletLoginResponse;
import com.ruoyi.common.utils.bean.WeChatAppletUserInfo;
import com.ruoyi.common.utils.bean.WechatSetting;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.spec.AlgorithmParameterSpec;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 微信小程序工具类
 *
 * @author SK
 * @since 2018/6/13
 */
@Slf4j
public class WeChatAppletUtils {

    /**
     * 获取登录信息地址
     */
    private final static String GET_LOGIN_INFO_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    /**
     * 获取微信小程序access_token地址（参数为 appid 和 secret）
     */
    private final static String GET_WECHAT_APPLET_ACCESS_TOKEN_ = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
    /**
     * 获取微信小程序分享码请求地址
     */
    private final static String GET_WECHAT_APPLET_SHARE_CODE_URL = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";
    /**
     * 获取微信小程序直播房间列表地址
     */
    private final static String GET_WECHAT_APPLET_LIVE_PLAYER_LIST_URL = "https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=%s";
    // 获取小程序二维码 接口B：适用于需要的码数量极多的业务场景
    private final static String WXACODE_UNLIMIT = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";

    private WeChatAppletUtils() {
    }

    /**
     * 获取二维码
     */
    public static String getWxacodeUrl(String page, String scene, WechatSetting wechatSetting, String filePath, int retryTime) {
        // 定义返回值
        String result = null;

        String imgName = UUID.randomUUID() + ".jpg";

        JSONObject data = new JSONObject();
        data.put("width", 800); // 二维码的宽度
        data.put("auto_color", false); // 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调

        Map<String, Object> line_color = new HashMap<>(); // auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
        line_color.put("r", 0);
        line_color.put("g", 0);
        line_color.put("b", 0);
        data.put("line_color", line_color);

        data.put("is_hyaline", false); // 是否需要透明底色,is_hyaline 为true时，生成透明底色的小程序码
        if (StringUtil.isNotEmpty(scene)) {
            data.put("scene", scene); // 场景值 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
        }
        if (StringUtil.isNotEmpty(page)) {
            data.put("page", page);// 必须是已经发布的小程序存在的页面（否则报错），例如 "pages/index/index" ,根路径前不要填加'/',不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
        }
        String accessToken = getAccessToken(wechatSetting);
        HttpHeaders headers = new HttpHeaders();
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            org.springframework.http.HttpEntity<String> httpEntity = new org.springframework.http.HttpEntity<>(data.toJSONString(), headers);
            ResponseEntity<byte[]> response = restTemplate.exchange(String.format(WechatConfig.API.WXACODE_UNLIMIT, accessToken), HttpMethod.POST, httpEntity, byte[].class);
            HttpHeaders httpHeaders = response.getHeaders();
            List<String> contentTypeList = httpHeaders.get(HttpHeaders.CONTENT_TYPE);

            if (contentTypeList != null && contentTypeList.size() > 0 && contentTypeList.contains("image/jpeg")) {
                log.info("返回的是图片");

                inputStream = new ByteArrayInputStream(response.getBody());
                result = new StringBuilder().append("upload/mini/").append(imgName).toString();
                File file = new File(new StringBuilder().append(filePath).append(result).toString());


                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                outputStream = new FileOutputStream(file);
                int len = 0;
                byte[] buf = new byte[1024];
                while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                    outputStream.write(buf, 0, len);
                }
                outputStream.flush();
                Map<String, Object> map = AliOSSUtils.uploadFileImgUrl(result, file);
                result = map.get("imgUrl").toString();
                log.info(result);
                log.info("二维码生成 success");
            } else {
                java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
                String jsonString = encoder.encodeToString(response.getBody());

                /*try {
                    log.info("返回的是json信息: " + Base64ObjectUtil.base64ToString(jsonString));
                } catch (Exception e) {
                    log.info("json解析失败:" + jsonString);
                }*/

                if (retryTime == 0) {
                    result = getWxacodeUrl(page, scene, wechatSetting, filePath, 1);
                } else {
                    result = "";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "";
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }

        return result;
    }

    /**
     * 获取二维码
     */
    public static String getWxacodeUrl(String page, String scene, String accessToken, String filePath, RedisCache redisUtils, int retryTime) {
        // 定义返回值
        String result = null;

        String imgName = UUID.randomUUID() + ".jpg";

        JSONObject data = new JSONObject();
        data.put("width", 800); // 二维码的宽度
        data.put("auto_color", false); // 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调

        Map<String, Object> line_color = new HashMap<>(); // auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
        line_color.put("r", 0);
        line_color.put("g", 0);
        line_color.put("b", 0);
        data.put("line_color", line_color);

        data.put("is_hyaline", false); // 是否需要透明底色,is_hyaline 为true时，生成透明底色的小程序码
        if (StringUtil.isNotEmpty(scene)) {
            data.put("scene", scene); // 场景值 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
        }
        if (StringUtil.isNotEmpty(page)) {
            data.put("page", page);// 必须是已经发布的小程序存在的页面（否则报错），例如 "pages/index/index" ,根路径前不要填加'/',不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
        }

        HttpHeaders headers = new HttpHeaders();
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            org.springframework.http.HttpEntity<String> httpEntity = new org.springframework.http.HttpEntity<>(data.toJSONString(), headers);
            ResponseEntity<byte[]> response = restTemplate.exchange(String.format(GET_WECHAT_APPLET_SHARE_CODE_URL, accessToken), HttpMethod.POST, httpEntity, byte[].class);
            HttpHeaders httpHeaders = response.getHeaders();
            List<String> contentTypeList = httpHeaders.get(HttpHeaders.CONTENT_TYPE);

            if (contentTypeList != null && contentTypeList.size() > 0 && contentTypeList.contains("image/jpeg")) {
                log.info("返回的是图片");

                inputStream = new ByteArrayInputStream(response.getBody());
                result = new StringBuilder().append("upload/mini/").append(imgName).toString();
                File file = new File(new StringBuilder().append(filePath).append(result).toString());


                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                outputStream = new FileOutputStream(file);
                int len = 0;
                byte[] buf = new byte[1024];
                while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                    outputStream.write(buf, 0, len);
                }
                outputStream.flush();
                Map<String, Object> map = AliOSSUtils.uploadFileImgUrl(result, file);
                result = map.get("imgUrl").toString();
                log.info(result);
                log.info("二维码生成 success");
            } else {
                java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
                String jsonString = encoder.encodeToString(response.getBody());

                try {
                    // log.info("返回的是json信息: " + Base64ObjectUtil.base64ToString(jsonString));
                } catch (Exception e) {
                    log.info("json解析失败:" + jsonString);
                }

                if (retryTime == 0) {
                    // 强制刷新token 生产二维码
                    accessToken = refreshGlobalToken(redisUtils);
                    log.info("强制刷新token: " + accessToken);

                    result = getWxacodeUrl(page, scene, accessToken, filePath, redisUtils, 1);
                } else {
                    result = "";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "";
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }

        return result;
    }

    /**
     * 强制刷新token
     */
    private static String refreshGlobalToken(RedisCache redisUtil) {
        String token = "";
        if (StringUtil.isEmpty(token)) {
            String respToken = WechatUtils.executeHttpGet(String.format(WechatConfig.API.ACCESS_TOKEN, WechatConfig.MINI.APP_ID, WechatConfig.MINI.APP_SECRET));
            Map tokenMap = MapUtil.json2Map(respToken);
            token = (String) tokenMap.get("access_token");
            redisUtil.putToRedis(KeysConstant.WX.ACCESS_TOKEN, token, (long) 6000, TimeUnit.SECONDS);
        }
        return token;
    }

    // 步数转卡路里
    public static float getDistanceByStep(long steps) {
        return steps * 0.6f / 1000;
    }

    /**
     * 微信解密运动步数
     *
     * @param sessionKey
     * @param encryptedData
     * @param iv
     * @return
     */
    public static String decryptWeChatRunInfo(String sessionKey, String encryptedData, String iv) {
        String result = null;
        byte[] encrypData = Base64.decodeBase64(encryptedData);
        byte[] ivData = Base64.decodeBase64(iv);
        byte[] sessionKeyB = Base64.decodeBase64(sessionKey);
        try {
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivData);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keySpec = new SecretKeySpec(sessionKeyB, "AES");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] doFinal = cipher.doFinal(encrypData);
            result = new String(doFinal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 获取登录信息
     *
     * @param code          用户登录凭证（有效期五分钟）。开发者需要在开发者服务器后台调用 api，使用 code 换取 openid 和 session_key 等信息
     * @param wechatSetting 微信设置
     * @return 微信小程序登录返回实体
     */
    public static WeChatAppletLoginResponse getLoginInfo(String code, WechatSetting wechatSetting) {
        log.debug("getLoginInfo and code:{} \r\n wechatSetting:{}", code, wechatSetting);
        String json = WechatUtils.executeHttpGet(String.format(GET_LOGIN_INFO_URL, wechatSetting.getAppId(), wechatSetting.getAppSecret(), code));
        log.debug("getLoginInfo and json:{}", json);
        WeChatAppletLoginResponse weChatAppletLoginResponse = JSON.parseObject(json, WeChatAppletLoginResponse.class);
        if (Objects.isNull(weChatAppletLoginResponse)) {
            log.error("getLoginInfo fail : weChatAppletLoginResponse is null");
            return null;
        }
        if (weChatAppletLoginResponse.isError()) {
            log.debug("getLoginInfo fail and errorMsg:{}", weChatAppletLoginResponse.getErrmsg());
            return null;
        }
        return weChatAppletLoginResponse;
    }

    /**
     * 根据用户信息获取unionId
     *
     * @param sessionKey           会话密钥
     * @param weChatAppletUserInfo 微信小程序用户信息实体
     * @return 用户unionId
     */
    public static String getUnionIdFromUserInfo(String sessionKey, WeChatAppletUserInfo weChatAppletUserInfo) {
        log.debug("getUnionIdFromUserInfo and sessionKey:{} \r\n weChatAppletUserInfo:{}", sessionKey, weChatAppletUserInfo);
        String unionId = null;
        if (Objects.isNull(weChatAppletUserInfo)) {
            log.error("getUnionIdFromUserInfo fail:weChatAppletUserInfo is null ");
            return null;
        }
        // 被加密的数据
        byte[] dataByte = Base64.decodeBase64(weChatAppletUserInfo.getEncryptedData());
        // 加密秘钥
        byte[] aeskey = Base64.decodeBase64(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decodeBase64(weChatAppletUserInfo.getIv());

        try {
            byte[] resultByte = AesUtils.getInstance().decrypt(dataByte, aeskey, ivByte);
            String userInfo = new String(resultByte, "UTF-8");
            unionId = JSON.parseObject(userInfo).getString("unionId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unionId;
    }

    /**
     * 获取微信小程序access_token（有效期两小时）
     *
     * @param wechatSetting 微信设置实体
     * @return 微信小程序access_token
     */
    public static String getAccessToken(WechatSetting wechatSetting) {
        log.debug("getAccessToken and wechatSetting:{} ", wechatSetting);
        String accessTokenForShareUrl = String.format(GET_WECHAT_APPLET_ACCESS_TOKEN_, wechatSetting.getAppId(), wechatSetting.getAppSecret());
        JSONObject res = JSON.parseObject(CustomHttpUtils.doGet(accessTokenForShareUrl));
        if (!StringUtils.isEmpty(res.getString("errcode"))) {
            if (!StringUtils.isEmpty(res.getString("errmsg"))) {
                log.error("getAccessToken Fail and errmsg:{}", res.getString("errmsg"));
            }
            return null;
        } else {
            return res.getString("access_token");
        }
    }

    /**
     * 获取json参数post请求返回值（微信小程序码）
     *
     * @param url        请求地址
     * @param jsonString 请求参数
     * @return 请求返回（ByteArrayInputStream格式，访问数组的字节输入流）
     */
    public static ByteArrayInputStream getJsonRequestResult(String url, String jsonString) {
        log.debug("getWeChatAppletCode and url :{} \r\n jsonString :{} ", url, jsonString);
        String result = null;
        InputStream inputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        BasicHttpClientConnectionManager connManager = new BasicHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", SSLConnectionSocketFactory.getSocketFactory())
                        .build(),
                null,
                null,
                null
        );
        HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(connManager)
                .build();
        HttpPost httpRequest = new HttpPost(url);
        httpRequest.setHeader("Content-type", "application/json; charset=utf-8");
        StringEntity requestParam = new StringEntity(jsonString, "UTF-8");
        requestParam.setContentType("application/json");
        try {
            httpRequest.setEntity(requestParam);
            HttpResponse response = httpClient.execute(httpRequest);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                // 创建一个Buffer字符串
                byte[] buffer = new byte[1024];
                // 每次读取的字符串长度，如果为-1，代表全部读取完毕
                int len = 0;
                // 使用一个输入流从buffer里把数据读取出来
                while ((len = inputStream.read(buffer)) != -1) {
                    // 用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
                    outStream.write(buffer, 0, len);
                }
                // 关闭输入流
                inputStream.close();
                // 把outStream里的数据写入内存
                byteArrayInputStream = new ByteArrayInputStream(outStream.toByteArray());
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        log.debug("getJsonRequestResult and result:{}", result == null ? "" : result);
        return byteArrayInputStream;
    }

    /**
     * 数组的字节输入流转化为base64字符串
     *
     * @param inputStream 输入流
     * @return base64字符串
     */
    public static String getBase64FromInputStream(InputStream inputStream) {
        // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;
        // 读取图片字节数组
        try {
            if (Objects.isNull(inputStream) || inputStream.available() <= 200) {
                log.error("getBase64FromInputStream fail due to inputStream is null");
                return null;
            }
            ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
            byte[] buff = new byte[100];
            int rc = 0;
            while ((rc = inputStream.read(buff, 0, 100)) > 0) {
                swapStream.write(buff, 0, rc);
            }
            data = swapStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return java.util.Base64.getEncoder().encodeToString(data);
    }

    /**
     * 获取微信小程序分享码请求地址
     *
     * @param accessToken 微信小程序access_token
     * @return 微信小程序分享码请求地址
     */
    public static String getWeChatAppletShareCodeUrl(String accessToken) {
        if (StringUtils.isEmpty(accessToken)) {
            log.error("getWeChatAppletShareCodeUrl fail due to accessToken is null");
            return null;
        }
        return String.format(GET_WECHAT_APPLET_SHARE_CODE_URL, accessToken);
    }

    /**
     * 获取微信小程序直播列表地址
     *
     * @param accessToken 微信小程序access_token
     * @return 微信小程序直播列表地址
     */
    public static String getWeChatAppletLivePlayerListUrl(String accessToken) {
        if (StringUtils.isEmpty(accessToken)) {
            log.error("getWeChatAppletLivePlayerListUrl fail due to accessToken is null");
            return null;
        }
        return String.format(GET_WECHAT_APPLET_LIVE_PLAYER_LIST_URL, accessToken);
    }

    public static void main(String[] args) {
        String accessTokenForShareUrl = String.format(GET_WECHAT_APPLET_ACCESS_TOKEN_, "wx6007249d6b2348ea", "1b1c5e732f38a03c980061855e68a19a");
        JSONObject res = JSON.parseObject(CustomHttpUtils.doGet(accessTokenForShareUrl));
        if (!StringUtils.isEmpty(res.getString("errcode"))) {
            if (!StringUtils.isEmpty(res.getString("errmsg"))) {
                log.error("getAccessToken Fail and errmsg:{}", res.getString("errmsg"));
            }

        } else {
            String token = res.getString("access_token");
            String url = "https://api.weixin.qq.com/datacube/getweanalysisappiddailysummarytrend?access_token=%s";
            WechatParam param = new WechatParam();
            param.setBegin_date("20210106");
            param.setEnd_date("20210106");
            String json = CustomHttpUtils.postJson(String.format(url, token), JSON.toJSONString(param));
            System.out.printf(json);

        }
    }
}
