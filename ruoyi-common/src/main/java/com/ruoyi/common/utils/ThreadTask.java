/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:ThreadTask.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by 魔金商城 on 17/6/20.
 * 线程池
 */
@Slf4j
public class ThreadTask {

    private static final ThreadTask threadTask = new ThreadTask();
    /**
     * 固定线程池
     */
    private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private ThreadTask() {
    }

    public static ThreadTask getInstance() {
        return threadTask;
    }

    /**
     * 加入线程池执行
     *
     * @param runnable 执行线程
     */
    public void addTask(Runnable runnable) {
        try {
            executorService.execute(runnable);
        } catch (Exception e) {
            log.error("ThreadPool execute error", e);
        }
    }
}