/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:RefundRequest.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.utils.bean;

import lombok.Data;

/**
 * Created by 魔金商城 on 18/2/7.
 * 提现请求实体
 */
@Data
public class RefundRequest {

    /**
     * 交流流水号
     */
    private String tradeNo;

    /**
     * 退款理由
     */
    private String refundReason;

    /**
     * 提现金额
     */
    private String money;

    private String refuseNo;
}
