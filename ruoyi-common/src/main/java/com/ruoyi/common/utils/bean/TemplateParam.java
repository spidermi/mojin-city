/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:TemplateParam.java
 * Date:2021/01/13 20:43:13
 */

package com.ruoyi.common.utils.bean;

public class TemplateParam {

    private String key;
    private String value;

    public TemplateParam(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}