/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:CaptchaExpireException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.exception.user;

/**
 * 验证码失效异常类
 *
 * @author ruoyi
 */
public class CaptchaExpireException extends UserException {
    private static final long serialVersionUID = 1L;

    public CaptchaExpireException() {
        super("user.jcaptcha.expire", null);
    }
}
