/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:ServiceException.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.exception;

import com.ruoyi.common.md5.MessageSourceUtil;
import lombok.Data;

/**
 * Created by 魔金商城 on 17/7/10.
 * 业务异常
 */
@Data
public class ServiceException extends RuntimeException {

    /**
     * 错误code
     */
    private String errorCode;

    public ServiceException() {

    }

    public ServiceException(String errorCode) {
        this(errorCode, MessageSourceUtil.getMessage(errorCode));
    }

    public ServiceException(String errorCode, String msg) {
        super(msg == null ? errorCode : msg);
        this.errorCode = errorCode;
    }

}
