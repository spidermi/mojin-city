/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:BusinessStatus.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.enums;

/**
 * 操作状态
 *
 * @author ruoyi
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
