/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:BaseEnum.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.enums;

public interface BaseEnum<K> {
    K code();

    String desc();
}
