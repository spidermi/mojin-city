/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:OperatorType.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.enums;

/**
 * 操作人类别
 *
 * @author ruoyi
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
