/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:DataSourceType.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.common.enums;

/**
 * 数据源
 *
 * @author ruoyi
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
