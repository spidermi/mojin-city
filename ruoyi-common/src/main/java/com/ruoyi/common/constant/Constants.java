/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:Constants.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.common.constant;

/**
 * 通用常量信息
 *
 * @author ruoyi
 */
public class Constants {

    //订单操作redis队列
    public static final String GOODS_DOWN = "GOODS_DOWN"; // 商品下降通知

    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_STORE_PREFIX = "BearerStore ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_STORE_USER_KEY = "login_user_store_key";


    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户名称
     */
    public static final String JWT_USERNAME = "sub";

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

    /**
     * 请求header中的微信用户端源链接参数
     */
    public static final String WX_CLIENT_HREF_HEADER = "wx-client-href";

    /** 启用状态 */
    public static final String NORMAL_CODE=new String("0");


    /** 删除状态 */
    public static final String DELETE_CODE=new String("2");

    /** 业务判断成功状态 */
    public static final Integer SERVICE_RETURN_SUCCESS_CODE=0;


    /** 系统用户 */
    public static final String USER_TYPE_SYS="00";

    /** 企业微信用户 */
    public static final  String USER_TYPE_WECOME="11";


    /** 企业管理 */
    public static  final String USER_TYOE_CORP_ADMIN="22";


    /** 企业微信用户系统中默认用户 */
    public static final String DEFAULT_WECOME_ROLE_KEY="WeCome";


    /** 企业微信用户系统中默认用户 */
    public static final String DEFAULT_WECOME_CORP_ADMIN="CROP_ADMIN_ROLE";

    /**完成待办*/
    public static final String HANDLE_SUCCESS="3";
    /**
     * 英文符号
     */
    public final static String SYMBOL_COMMA = ",";
    public final static String SYMBOL_POINT = ".";
    public final static String SYMBOL_QUESTION = "?";
    public final static String SYMBOL_COLON = ":";
    public final static String SYMBOL_STAR = "*";
    public final static String SYMBOL_WELL = "#";
    public final static String SYMBOL_HYPHEN = "-";
    public final static String SYMBOL_UNDERLINE = "_";
    public final static String SYMBOL_LEFT_BRACKET = "{";
    public final static String SYMBOL_RIGHT_BRACKET = "}";
    public final static String SYMBOL_RIGHT_EQUAL = "=";
    public final static String SYMBOL_LEFT_OBLIQUE_LINE = "/";
    //物流  https://market.aliyun.com/products/56928004/cmapi021863.html#sku=yuncode15863000015
    public static String LOGISTICS_API_URL = "https://wuliu.market.alicloudapi.com/kdi";
}
