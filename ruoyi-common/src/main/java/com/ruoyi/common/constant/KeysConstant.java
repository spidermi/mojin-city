package com.ruoyi.common.constant;

/**
 * Created by hou on 21/03/2017.
 */
public interface KeysConstant {

    String SHARE_CODE_QR = "shareCodeQR:%s:%s";

    interface SMS_CODE {
        String APP_BIND_PHONE = "appBindPhoneSmsCode:%s:%s";
    }

    interface SESSION_KEY {
        String APP = "appSessionKey:%s";
        String WX = "wxSessionKey:%s";
        String ADMIN = "adminSessionKey:%s:%s";
        String ADMINROLE = "adminRoleSessionKey:%s:%s";
    }

    interface SHOPGOODS {
        String SECKILL_SHOPGOODS_STOCK = "secKillShopGoodsStockKey:%s"; //库存
        String SECKILL_SHOPGOODS_STORE_STOCK = "secKillShopGoodsStoreStockKey:%s:%s"; //门店库存  门店id:商品id
        String SECKILL_SHOPGOODS_MAXBUY = "secKillShopGoodsMaxBuyKey:%s"; //商品限购
        String SECKILL_SHOPGOODS_BUYER = "secKillShopGoodsStockKey:%s:%s";//用户限购计数
    }

    interface ORDER {
        String ORDER_CODE = "orderCodeKey:%s";
    }


    interface QRCODE {
        String STORE_QRCODE = "storeQrCode:%s";//店员的code,每个店员/店长持有一个
        String STORE_QRCODE_VALIUE = "storeQrCodeValue:%s";//店员的code,每个店员/店长持有一个
    }

    interface WX {
        String ACCESS_TOKEN = "miniAccessToken";
    }


    /**
     * 任务中心-redisKeys
     */
    interface TASK_CENTER {
        String TASK_BROWSE_GOODS_CONFIG = "taskCenter:browseGoodsConfig";//任务中心-浏览商品配置
        String TASK_BROWSE_GOODS_RECORD = "taskCenter:browseGoodsRecord:%s:%s";//任务中心-用户浏览商品记录 userId,goodsId
        String TASK_BROWSE_GOODS_RECORD_NUM = "taskCenter:browseGoods:%s:num";//任务中心-用户浏览商品-任务完成数 userId ，完成数
        String TASK_BROWSE_GOODS_STATUS = "taskCenter:browseGoods:%s:status";//任务中心-用户浏览商品-任务状态 userId


        String TASK_SHARE_GOODS_CONFIG = "taskCenter:shareGoodsConfig";//任务中心-分享商品配置
        String TASK_SHARE_GOODS_RECORD = "taskCenter:shareGoodsRecord:%s:%s";//任务中心-用户分享商品记录 userId,goodsId
        String TASK_SHARE_GOODS_RECORD_NUM = "taskCenter:shareGoods:%s:num";//任务中心-用户分享商品-任务完成数 userId ，完成数
        String TASK_SHARE_GOODS_STATUS = "taskCenter:shareGoods:%s:status";//任务中心-用户分享商品-任务状态 userId


        String TASK_BIND_LOWER_CONFIG = "taskCenter:bindLowerConfig"; //任务中心-绑定下级配置
        String TASK_BIND_LOWER_RECORD_NUM = "taskCenter:bindLower:%s:num";//任务中心-绑定下级-任务完成数 userId ，完成数
        String TASK_BIND_LOWER_STATUS = "taskCenter:bindLower:%s:status";//任务中心-绑定下级-任务状态 userId


        String TASK_BUY_GOODS_CONFIG = "taskCenter:buyGoodsConfig"; //任务中心-购买商品配置
        String TASK_BUY_GOODS_RECORD_NUM = "taskCenter:buyGoods:%s:num";//任务中心-购买商品-任务完成数 userId ，完成数
        String TASK_BUY_GOODS_STATUS = "taskCenter:buyGoods:%s:status";//任务中心-购买商品-任务状态 userId


        String TASK_BUY_SINCE_STORE_CONFIG = "taskCenter:buySinceStoreConfig"; //任务中心-门店自提配置
        String TASK_BUY_SINCE_STORE_RECORD_NUM = "taskCenter:buySinceStore:%s:num";//任务中心-门店自提-任务完成数 userId ，完成数
        String TASK_BUY_SINCE_STORE_STATUS = "taskCenter:buySinceStore:%s:status";//任务中心-门店自提-任务状态 userId
    }

    interface CURRENT_LIMITING {
        String SECKILL_INTERCEPT = "seckillIntercept";
        String SECKILL_INTERCEPTNUM = "seckillInterceptNum";
    }
}
