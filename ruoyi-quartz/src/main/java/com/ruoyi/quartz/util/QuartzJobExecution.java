/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:QuartzJobExecution.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.quartz.util;

import com.ruoyi.quartz.domain.SysJob;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（允许并发执行）
 *
 * @author ruoyi
 */
public class QuartzJobExecution extends AbstractQuartzJob {
    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
