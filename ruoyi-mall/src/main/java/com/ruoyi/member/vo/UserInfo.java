/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:UserInfo.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;

import lombok.Data;

/**
 * 小程序redis参数返回实体类
 *
 * @author SK
 * @since 2018/6/13
 */
@Data
public class UserInfo {

    /**
     * 小程序sessionKey
     */
    private String nickName;

    /**
     * 1 男
     */
    private String gender;

    /**
     * 头条小程序
     */
    private String userId;

    /**
     * 联合登录id
     */
    private String city;

    /**
     * 用户id
     */
    private String province;
    private String country;

    /**
     * 微信用户标识
     */
    private String avatarUrl;


}
