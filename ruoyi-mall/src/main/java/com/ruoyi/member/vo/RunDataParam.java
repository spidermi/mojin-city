/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:RunDataParam.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;

import lombok.Data;

@Data
public class RunDataParam {
    private String sessionKey;
    private String encryptedData;
    private String iv;
}
