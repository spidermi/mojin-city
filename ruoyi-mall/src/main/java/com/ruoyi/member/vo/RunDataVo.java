/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:RunDataVo.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;

import lombok.Data;

import java.util.Date;

@Data
public class RunDataVo {
    private Long timestamp;
    private Long step;
    private Date createTime;
}
