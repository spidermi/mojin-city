/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WxPhoneInfo.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;

import lombok.Data;

/**
 * 解析微信授权获取手机号信息，解密后内容
 */
@Data
public class WxPhoneInfo {
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    private WaterMark watermark;


    @Data
    class WaterMark {
        private long timestamp;
        private String appid;


    }
}
