/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:WechatResponse.java
 * Date:2020/11/27 16:26:27
 */

package com.ruoyi.member.vo;

import lombok.Data;

/**
 * 社区团购微信分享签名实体类
 *
 * @author Caizize created on 2019/5/7
 */
@Data
public class WechatResponse {

    /**
     * 公众号的唯一标识
     */
    private String appId;

    /**
     * 生成签名的时间戳，精确到秒
     */
    private String timestamp;

    /**
     * 生成签名的随机串
     */
    private String nonceStr;

    /**
     * 签名
     */
    private String signature;


    /**
     * 构建社区团购微信分享签名实体类
     *
     * @param appId     公众号的唯一标识
     * @param timestamp 生成签名的时间戳
     * @param nonceStr  生成签名的随机串
     * @param signature 签名
     * @return 社区团购微信分享签名实体类
     */
    public static WechatResponse buildCommunityBuyWechatResponse(String appId, String timestamp, String nonceStr, String signature) {
        WechatResponse communityBuyWechatResponse = new WechatResponse();
        communityBuyWechatResponse.appId = appId;
        communityBuyWechatResponse.timestamp = timestamp;
        communityBuyWechatResponse.nonceStr = nonceStr;
        communityBuyWechatResponse.signature = signature;
        return communityBuyWechatResponse;
    }

}
