package com.ruoyi.tc.vo;

import com.ruoyi.tc.domain.TcNav;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class TcMemberLevelVo  {
    private String month;
    private Double price;
    private long id;
}
