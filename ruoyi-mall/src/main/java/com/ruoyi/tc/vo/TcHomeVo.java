package com.ruoyi.tc.vo;


import com.ruoyi.tc.domain.TcAd;
import com.ruoyi.tc.domain.TcNav;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("首页数据")
public class TcHomeVo {
    String app_name;

    List<TcAd> car_banner;

    List<TcAd> find_banner;

    List<TcNav> find_menu;

    TcAd home_ad;
    List<TcAd> home_banner;
    List<TcNav> home_menu;


}
