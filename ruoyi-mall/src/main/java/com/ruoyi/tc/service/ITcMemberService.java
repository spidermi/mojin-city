/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:ITcMemberService.java
 * Date:2021/01/11 21:00:11
 */

package com.ruoyi.tc.service;

import com.ruoyi.common.core.domain.AjaxResult;




import com.ruoyi.tc.domain.TcMember;
import com.ruoyi.util.PageHelper;

import java.math.BigDecimal;
import java.util.List;

/**
 * 会员Service接口
 *
 * @author 魔金商城
 * @date 2020-07-25
 */
public interface ITcMemberService {
    /**
     * 查询会员
     *
     * @param id 会员ID
     * @return 会员
     */
    public TcMember selectTcMemberById(Long id);

    public TcMember queryCustomerByh5OpenId(String openid, long storeId);

    public TcMember queryCustomerByappOpenId(String openid, long storeId);

    public TcMember queryCustomerByappletOpenId(String openid, long storeId);

    /**
     * 查询会员列表
     *
     * @param TcMember 会员
     * @return 会员集合
     */
    public List<TcMember> selectTcMemberList(TcMember TcMember);

    /**
     * 新增会员
     *
     * @param TcMember 会员
     * @return 结果
     */
    public int insertTcMember(TcMember TcMember);

    /**
     * 修改会员
     *
     * @param TcMember 会员
     * @return 结果
     */
    public int updateTcMember(TcMember TcMember);

    /**
     * 批量删除会员
     *
     * @param ids 需要删除的会员ID
     * @return 结果
     */
    public int deleteTcMemberByIds(Long[] ids);

    /**
     * 删除会员信息
     *
     * @param id 会员ID
     * @return 结果
     */
    public int deleteTcMemberById(Long id);


    /**
     * 根据会员id查询会员信息(没有密码信息)
     *
     * @param id 会员id
     * @return 返回会员信息
     */
    TcMember queryCustomerWithNoPasswordById(long id);


    /**
     * 根据会员id查询会员信息(包含密码信息)
     *
     * @param id 会员id
     * @return 返回会员信息
     */
    TcMember queryCustomerInfoById(long id);


    /**
     * 根据店铺id查询会员信息和会员等级
     *
     * @param id 会员id
     * @return 会员信息
     */
    TcMember queryCustomerWithCustomerLevel(long id);


    /**
     * 新增会员信息
     *
     * @param customer 会员
     * @return 成功返回1 失败返回0
     */
    int addCustomer(TcMember customer);

    /**
     * 自动添加用户 用于免密登录场景
     *
     * @param customer 用户对象
     * @return 主键id
     */
    Long autoAddCustomer(TcMember customer);

    /**
     * 修改会员信息
     *
     * @param customer 会员信息
     * @return 成功返回1 失败返回0 -2 已经绑定过推荐码 -3 推荐人不存在 -4 推荐人不能是自己
     */
    int updateCustomer(TcMember customer);


    /**
     * 根据用户名称查询用户信息
     *
     * @param userName 用户名称(可能是用户名 , 邮箱或者手机号码)
     * @return 返回用户信息
     */
    TcMember queryCustomerByName(String userName, long storeId);


    /**
     * 更新登录时间
     *
     * @param customerId 会员id
     * @return 成功返回1  失败返回0
     */
    int updateLoginTime(long customerId);

    /**
     * 校验手机号码是否存在
     *
     * @param mobile 手机号码
     * @return 存在返回>0  不存在返回0
     */
    int isMobileExist(String mobile, long storeId);


    /**
     * 校验邮箱是否存在
     *
     * @param mobile 手机号码
     * @return 存在返回>0  不存在返回0
     */
    int isEmailExist(String mobile, long storeId);

    /**
     * 修改用户密码
     *
     * @param customerId 用户id
     * @param password   密码
     * @return 成功返回1 失败返回0
     */
    int updatePassword(long customerId, String password);

    /**
     * 修改用户支付密码
     *
     * @param customerId  用户id
     * @param payPassword 支付密码
     * @return 成功返回1 失败返回0
     */
    int updatePayPassword(long customerId, String payPassword);

    /**
     * 用户绑定新的手机号码
     *
     * @param customerId 用户id
     * @param mobile     手机号码
     * @return 成功返回1  失败返回0
     */
    int bindNewMobile(long customerId, String mobile);


    /**
     * 修改用户密码
     *
     * @param mobile   用户手机号码
     * @param password 密码
     * @return 成功返回1 失败返回0
     */
    int updatePasswordByMobile(String mobile, String password, long storeId);

    /**
     * 修改用户总的消费金额
     *
     * @param customerId 会员id
     * @param orderMoney 订单金额
     * @return 成功返回1 失败返回0
     */
    int updateCustomerConsumptionAmount(long customerId, BigDecimal orderMoney);

    /**
     * 修改用户总的消费金额(admin端用)
     *
     * @param customerId 会员id
     * @param money      更改金额
     * @return 成功返回1 失败返回0 -1:更改后金额小于0
     */
    int updateCustomerConsumptionAmountByAdmin(long customerId, BigDecimal money);


    /**
     * 更新签到天数
     *
     * @param customerId   用户id
     * @param continueFlag 连续签到标记 true表示连续签到，否则为断签
     * @return 1:成功
     */
    int updateSignNum(long customerId, boolean continueFlag);

    /**
     * 修改推荐人的佣金
     *
     * @param customerId 推荐人会员id
     * @param commission 佣金
     * @return 成功返回>0
     */
    int updateCustomerCommission(long customerId, BigDecimal commission);


    /**
     * 查找用户信息
     *
     * @param ids 用户id集合
     * @return 用户信息集合
     */
    List<TcMember> queryCustomersByIds(List<Long> ids, long storeId);

    /**
     * 查找所有用户信息
     */
    List<TcMember> queryAllCustomer();

    /**
     * 查询今日新增会员数
     *
     * @return 返回今日新增会员数
     */
    int queryNewCustomerToday();

    /**
     * 查询本周新增会员数
     *
     * @return 返回本周新增会员数
     */
    int queryNewCustomerThisWeek();



    /**
     * 查找所有没有开店的用户手机号
     *
     * @return 没有开店用户手机号集合
     */
    List<String> queryAllCustomerMobileForCreateStore();


    /**
     * 绑定用户推荐码
     *
     * @param recommendCode 推荐码
     * @param customerId    用户id
     * @return 1成功 -1 用户不存在 -2 已经绑定过推荐码 -3 推荐人不存在 -4 推荐人不能是自己
     */
    int bindCustomerRecommendCode(String recommendCode, long customerId);

    /**
     * 分页查询有下级的会员信息
     *
     * @param pageHelper 分页帮助类
     * @return 会员列表
     */
    PageHelper<TcMember> querySpreadCustomerByCustomerId(PageHelper<TcMember> pageHelper, long cusomerId);

    PageHelper<TcMember> querySpreadCustomerList(PageHelper<TcMember> pageHelper, long storeId);

    /**
     * 根据会员id查询分销下级会员
     *
     * @param customerId 会员id
     * @return 下级会员列表
     */
    List<TcMember> querySpreadCustomerByCustomerId(long customerId);

    /**
     * 根据会员id查询分销下级会员数量
     *
     * @param customerId 会员id
     * @return 下级会员数量
     */
    int querySpreadCustomerCountByCustomerId(long customerId);

    TcMember queryCustomerByRecommondCode(String code, long storeId);

    TcMember queryCustomerByAliOpenId(String userId, long storeId);



    int registerCustomer(long storeId, String mobile, String password, String code, String value, String recommendCode);
}
