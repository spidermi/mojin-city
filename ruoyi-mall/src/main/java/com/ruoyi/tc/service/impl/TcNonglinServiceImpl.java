package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcNonglin;
import com.ruoyi.tc.mapper.TcNonglinMapper;
import com.ruoyi.tc.service.ITcNonglinService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 农林牧渔Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcNonglinServiceImpl implements ITcNonglinService {

    @Autowired
    private TcNonglinMapper tcNonglinMapper;

    /**
     * 查询农林牧渔
     *
     * @param id 农林牧渔ID
     * @return 农林牧渔
     */
    @Override
    public TcNonglin selectTcNonglinById(Long id) {
        return tcNonglinMapper.selectTcNonglinById(id);
    }

    /**
     * 查询农林牧渔列表
     *
     * @param tcNonglin 农林牧渔
     * @return 农林牧渔
     */
    @Override
    public List<TcNonglin> selectTcNonglinList(TcNonglin tcNonglin) {
        return tcNonglinMapper.selectTcNonglinList(tcNonglin);
    }

    /**
     * 新增农林牧渔
     *
     * @param tcNonglin 农林牧渔
     * @return 结果
     */
    @Override
    public int insertTcNonglin(TcNonglin tcNonglin) {
        tcNonglin.setCreateTime(DateUtils.getNowDate());
        return tcNonglinMapper.insertTcNonglin(tcNonglin);
    }

    /**
     * 修改农林牧渔
     *
     * @param tcNonglin 农林牧渔
     * @return 结果
     */
    @Override
    public int updateTcNonglin(TcNonglin tcNonglin) {
        return tcNonglinMapper.updateTcNonglin(tcNonglin);
    }

    /**
     * 批量删除农林牧渔
     *
     * @param ids 需要删除的农林牧渔ID
     * @return 结果
     */
    @Override
    public int deleteTcNonglinByIds(Long[] ids) {
        return tcNonglinMapper.deleteTcNonglinByIds(ids);
    }

    /**
     * 删除农林牧渔信息
     *
     * @param id 农林牧渔ID
     * @return 结果
     */
    @Override
    public int deleteTcNonglinById(Long id) {
        return tcNonglinMapper.deleteTcNonglinById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcNonglinMapper.countByMonth(customerId);
    }
}
