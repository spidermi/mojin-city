package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcStroeComment;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcStroeCommentService {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public TcStroeComment selectTcStroeCommentById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcStroeComment 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TcStroeComment> selectTcStroeCommentList(TcStroeComment tcStroeComment);

    /**
     * 新增【请填写功能名称】
     *
     * @param tcStroeComment 【请填写功能名称】
     * @return 结果
     */
    public int insertTcStroeComment(TcStroeComment tcStroeComment);

    /**
     * 修改【请填写功能名称】
     *
     * @param tcStroeComment 【请填写功能名称】
     * @return 结果
     */
    public int updateTcStroeComment(TcStroeComment tcStroeComment);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcStroeCommentByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTcStroeCommentById(Long id);
}
