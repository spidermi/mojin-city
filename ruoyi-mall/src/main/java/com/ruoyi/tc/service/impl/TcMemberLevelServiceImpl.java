package com.ruoyi.tc.service.impl;

import com.ruoyi.tc.domain.TcMemberLevel;
import com.ruoyi.tc.mapper.TcMemberLevelMapper;
import com.ruoyi.tc.service.ITcMemberLevelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * vip等级Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcMemberLevelServiceImpl implements ITcMemberLevelService {

    @Autowired
    private TcMemberLevelMapper tcMemberLevelMapper;

    /**
     * 查询vip等级
     *
     * @param id vip等级ID
     * @return vip等级
     */
    @Override
    public TcMemberLevel selectTcMemberLevelById(Long id) {
        return tcMemberLevelMapper.selectTcMemberLevelById(id);
    }

    /**
     * 查询vip等级列表
     *
     * @param tcMemberLevel vip等级
     * @return vip等级
     */
    @Override
    public List<TcMemberLevel> selectTcMemberLevelList(TcMemberLevel tcMemberLevel) {
        return tcMemberLevelMapper.selectTcMemberLevelList(tcMemberLevel);
    }

    /**
     * 新增vip等级
     *
     * @param tcMemberLevel vip等级
     * @return 结果
     */
    @Override
    public int insertTcMemberLevel(TcMemberLevel tcMemberLevel) {
        return tcMemberLevelMapper.insertTcMemberLevel(tcMemberLevel);
    }

    /**
     * 修改vip等级
     *
     * @param tcMemberLevel vip等级
     * @return 结果
     */
    @Override
    public int updateTcMemberLevel(TcMemberLevel tcMemberLevel) {
        return tcMemberLevelMapper.updateTcMemberLevel(tcMemberLevel);
    }

    /**
     * 批量删除vip等级
     *
     * @param ids 需要删除的vip等级ID
     * @return 结果
     */
    @Override
    public int deleteTcMemberLevelByIds(Long[] ids) {
        return tcMemberLevelMapper.deleteTcMemberLevelByIds(ids);
    }

    /**
     * 删除vip等级信息
     *
     * @param id vip等级ID
     * @return 结果
     */
    @Override
    public int deleteTcMemberLevelById(Long id) {
        return tcMemberLevelMapper.deleteTcMemberLevelById(id);
    }
}
