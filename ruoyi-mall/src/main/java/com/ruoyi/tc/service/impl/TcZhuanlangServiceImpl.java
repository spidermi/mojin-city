package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcZhuanlang;
import com.ruoyi.tc.mapper.TcZhuanlangMapper;
import com.ruoyi.tc.service.ITcZhuanlangService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 转让Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcZhuanlangServiceImpl implements ITcZhuanlangService {

    @Autowired
    private TcZhuanlangMapper tcZhuanlangMapper;

    /**
     * 查询转让
     *
     * @param id 转让ID
     * @return 转让
     */
    @Override
    public TcZhuanlang selectTcZhuanlangById(Long id) {
        return tcZhuanlangMapper.selectTcZhuanlangById(id);
    }

    /**
     * 查询转让列表
     *
     * @param tcZhuanlang 转让
     * @return 转让
     */
    @Override
    public List<TcZhuanlang> selectTcZhuanlangList(TcZhuanlang tcZhuanlang) {
        return tcZhuanlangMapper.selectTcZhuanlangList(tcZhuanlang);
    }

    /**
     * 新增转让
     *
     * @param tcZhuanlang 转让
     * @return 结果
     */
    @Override
    public int insertTcZhuanlang(TcZhuanlang tcZhuanlang) {
        tcZhuanlang.setCreateTime(DateUtils.getNowDate());
        return tcZhuanlangMapper.insertTcZhuanlang(tcZhuanlang);
    }

    /**
     * 修改转让
     *
     * @param tcZhuanlang 转让
     * @return 结果
     */
    @Override
    public int updateTcZhuanlang(TcZhuanlang tcZhuanlang) {
        return tcZhuanlangMapper.updateTcZhuanlang(tcZhuanlang);
    }

    /**
     * 批量删除转让
     *
     * @param ids 需要删除的转让ID
     * @return 结果
     */
    @Override
    public int deleteTcZhuanlangByIds(Long[] ids) {
        return tcZhuanlangMapper.deleteTcZhuanlangByIds(ids);
    }

    /**
     * 删除转让信息
     *
     * @param id 转让ID
     * @return 结果
     */
    @Override
    public int deleteTcZhuanlangById(Long id) {
        return tcZhuanlangMapper.deleteTcZhuanlangById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcZhuanlangMapper.countByMonth(customerId);
    }
}
