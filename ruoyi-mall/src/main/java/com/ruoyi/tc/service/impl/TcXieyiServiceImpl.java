package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcXieyi;
import com.ruoyi.tc.mapper.TcXieyiMapper;
import com.ruoyi.tc.service.ITcXieyiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 标签Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcXieyiServiceImpl implements ITcXieyiService {

    @Autowired
    private TcXieyiMapper tcXieyiMapper;

    /**
     * 查询标签
     *
     * @param id 标签ID
     * @return 标签
     */
    @Override
    public TcXieyi selectTcXieyiById(Long id) {
        return tcXieyiMapper.selectTcXieyiById(id);
    }

    /**
     * 查询标签列表
     *
     * @param tcXieyi 标签
     * @return 标签
     */
    @Override
    public List<TcXieyi> selectTcXieyiList(TcXieyi tcXieyi) {
        return tcXieyiMapper.selectTcXieyiList(tcXieyi);
    }

    /**
     * 新增标签
     *
     * @param tcXieyi 标签
     * @return 结果
     */
    @Override
    public int insertTcXieyi(TcXieyi tcXieyi) {
        tcXieyi.setCreateTime(DateUtils.getNowDate());
        return tcXieyiMapper.insertTcXieyi(tcXieyi);
    }

    /**
     * 修改标签
     *
     * @param tcXieyi 标签
     * @return 结果
     */
    @Override
    public int updateTcXieyi(TcXieyi tcXieyi) {
        return tcXieyiMapper.updateTcXieyi(tcXieyi);
    }

    /**
     * 批量删除标签
     *
     * @param ids 需要删除的标签ID
     * @return 结果
     */
    @Override
    public int deleteTcXieyiByIds(Long[] ids) {
        return tcXieyiMapper.deleteTcXieyiByIds(ids);
    }

    /**
     * 删除标签信息
     *
     * @param id 标签ID
     * @return 结果
     */
    @Override
    public int deleteTcXieyiById(Long id) {
        return tcXieyiMapper.deleteTcXieyiById(id);
    }
}
