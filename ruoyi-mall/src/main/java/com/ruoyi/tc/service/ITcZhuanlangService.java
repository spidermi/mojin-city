package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcZhuanlang;

import java.util.List;

/**
 * 转让Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcZhuanlangService {
    /**
     * 查询转让
     *
     * @param id 转让ID
     * @return 转让
     */
    public TcZhuanlang selectTcZhuanlangById(Long id);

    /**
     * 查询转让列表
     *
     * @param tcZhuanlang 转让
     * @return 转让集合
     */
    public List<TcZhuanlang> selectTcZhuanlangList(TcZhuanlang tcZhuanlang);

    /**
     * 新增转让
     *
     * @param tcZhuanlang 转让
     * @return 结果
     */
    public int insertTcZhuanlang(TcZhuanlang tcZhuanlang);

    /**
     * 修改转让
     *
     * @param tcZhuanlang 转让
     * @return 结果
     */
    public int updateTcZhuanlang(TcZhuanlang tcZhuanlang);

    /**
     * 批量删除转让
     *
     * @param ids 需要删除的转让ID
     * @return 结果
     */
    public int deleteTcZhuanlangByIds(Long[] ids);

    /**
     * 删除转让信息
     *
     * @param id 转让ID
     * @return 结果
     */
    public int deleteTcZhuanlangById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
