package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcJob;
import com.ruoyi.tc.mapper.TcJobMapper;
import com.ruoyi.tc.service.ITcJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 招聘信息Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcJobServiceImpl implements ITcJobService {

    @Autowired
    private TcJobMapper tcJobMapper;

    /**
     * 查询招聘信息
     *
     * @param id 招聘信息ID
     * @return 招聘信息
     */
    @Override
    public TcJob selectTcJobById(Long id) {
        return tcJobMapper.selectTcJobById(id);
    }

    /**
     * 查询招聘信息列表
     *
     * @param tcJob 招聘信息
     * @return 招聘信息
     */
    @Override
    public List<TcJob> selectTcJobList(TcJob tcJob) {
        return tcJobMapper.selectTcJobList(tcJob);
    }

    /**
     * 新增招聘信息
     *
     * @param tcJob 招聘信息
     * @return 结果
     */
    @Override
    public int insertTcJob(TcJob tcJob) {
        tcJob.setCreateTime(DateUtils.getNowDate());
        return tcJobMapper.insertTcJob(tcJob);
    }

    /**
     * 修改招聘信息
     *
     * @param tcJob 招聘信息
     * @return 结果
     */
    @Override
    public int updateTcJob(TcJob tcJob) {
        return tcJobMapper.updateTcJob(tcJob);
    }

    /**
     * 批量删除招聘信息
     *
     * @param ids 需要删除的招聘信息ID
     * @return 结果
     */
    @Override
    public int deleteTcJobByIds(Long[] ids) {
        return tcJobMapper.deleteTcJobByIds(ids);
    }

    /**
     * 删除招聘信息信息
     *
     * @param id 招聘信息ID
     * @return 结果
     */
    @Override
    public int deleteTcJobById(Long id) {
        return tcJobMapper.deleteTcJobById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcJobMapper.countByMonth(customerId);
    }
}
