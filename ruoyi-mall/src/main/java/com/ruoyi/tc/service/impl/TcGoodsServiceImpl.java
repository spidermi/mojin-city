package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcGoods;
import com.ruoyi.tc.mapper.TcGoodsMapper;
import com.ruoyi.tc.service.ITcGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 物品信息Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcGoodsServiceImpl implements ITcGoodsService {

    @Autowired
    private TcGoodsMapper tcGoodsMapper;

    /**
     * 查询物品信息
     *
     * @param id 物品信息ID
     * @return 物品信息
     */
    @Override
    public TcGoods selectTcGoodsById(Long id) {
        return tcGoodsMapper.selectTcGoodsById(id);
    }

    /**
     * 查询物品信息列表
     *
     * @param tcGoods 物品信息
     * @return 物品信息
     */
    @Override
    public List<TcGoods> selectTcGoodsList(TcGoods tcGoods) {
        return tcGoodsMapper.selectTcGoodsList(tcGoods);
    }

    /**
     * 新增物品信息
     *
     * @param tcGoods 物品信息
     * @return 结果
     */
    @Override
    public int insertTcGoods(TcGoods tcGoods) {
        tcGoods.setCreateTime(DateUtils.getNowDate());
        return tcGoodsMapper.insertTcGoods(tcGoods);
    }

    /**
     * 修改物品信息
     *
     * @param tcGoods 物品信息
     * @return 结果
     */
    @Override
    public int updateTcGoods(TcGoods tcGoods) {
        return tcGoodsMapper.updateTcGoods(tcGoods);
    }

    /**
     * 批量删除物品信息
     *
     * @param ids 需要删除的物品信息ID
     * @return 结果
     */
    @Override
    public int deleteTcGoodsByIds(Long[] ids) {
        return tcGoodsMapper.deleteTcGoodsByIds(ids);
    }

    /**
     * 删除物品信息信息
     *
     * @param id 物品信息ID
     * @return 结果
     */
    @Override
    public int deleteTcGoodsById(Long id) {
        return tcGoodsMapper.deleteTcGoodsById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcGoodsMapper.countByMonth(customerId);
    }
}
