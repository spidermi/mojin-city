package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcUserCard;

import java.util.List;

/**
 * 人才简历Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcUserCardService {
    /**
     * 查询人才简历
     *
     * @param id 人才简历ID
     * @return 人才简历
     */
    public TcUserCard selectTcUserCardById(Long id);

    /**
     * 查询人才简历列表
     *
     * @param tcUserCard 人才简历
     * @return 人才简历集合
     */
    public List<TcUserCard> selectTcUserCardList(TcUserCard tcUserCard);

    /**
     * 新增人才简历
     *
     * @param tcUserCard 人才简历
     * @return 结果
     */
    public int insertTcUserCard(TcUserCard tcUserCard);

    /**
     * 修改人才简历
     *
     * @param tcUserCard 人才简历
     * @return 结果
     */
    public int updateTcUserCard(TcUserCard tcUserCard);

    /**
     * 批量删除人才简历
     *
     * @param ids 需要删除的人才简历ID
     * @return 结果
     */
    public int deleteTcUserCardByIds(Long[] ids);

    /**
     * 删除人才简历信息
     *
     * @param id 人才简历ID
     * @return 结果
     */
    public int deleteTcUserCardById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
