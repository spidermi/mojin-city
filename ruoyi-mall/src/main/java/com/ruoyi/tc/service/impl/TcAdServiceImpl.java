package com.ruoyi.tc.service.impl;

import com.ruoyi.tc.domain.TcAd;
import com.ruoyi.tc.mapper.TcAdMapper;
import com.ruoyi.tc.service.ITcAdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcAdServiceImpl implements ITcAdService {

    @Autowired
    private TcAdMapper tcAdMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public TcAd selectTcAdById(Long id) {
        return tcAdMapper.selectTcAdById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tcAd 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TcAd> selectTcAdList(TcAd tcAd) {
        return tcAdMapper.selectTcAdList(tcAd);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param tcAd 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTcAd(TcAd tcAd) {
        return tcAdMapper.insertTcAd(tcAd);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param tcAd 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTcAd(TcAd tcAd) {
        return tcAdMapper.updateTcAd(tcAd);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTcAdByIds(Long[] ids) {
        return tcAdMapper.deleteTcAdByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTcAdById(Long id) {
        return tcAdMapper.deleteTcAdById(id);
    }
}
