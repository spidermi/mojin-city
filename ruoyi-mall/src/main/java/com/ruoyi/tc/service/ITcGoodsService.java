package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcGoods;

import java.util.List;

/**
 * 物品信息Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcGoodsService {
    /**
     * 查询物品信息
     *
     * @param id 物品信息ID
     * @return 物品信息
     */
    public TcGoods selectTcGoodsById(Long id);

    /**
     * 查询物品信息列表
     *
     * @param tcGoods 物品信息
     * @return 物品信息集合
     */
    public List<TcGoods> selectTcGoodsList(TcGoods tcGoods);

    /**
     * 新增物品信息
     *
     * @param tcGoods 物品信息
     * @return 结果
     */
    public int insertTcGoods(TcGoods tcGoods);

    /**
     * 修改物品信息
     *
     * @param tcGoods 物品信息
     * @return 结果
     */
    public int updateTcGoods(TcGoods tcGoods);

    /**
     * 批量删除物品信息
     *
     * @param ids 需要删除的物品信息ID
     * @return 结果
     */
    public int deleteTcGoodsByIds(Long[] ids);

    /**
     * 删除物品信息信息
     *
     * @param id 物品信息ID
     * @return 结果
     */
    public int deleteTcGoodsById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
