package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcCar;
import com.ruoyi.tc.mapper.TcCarMapper;
import com.ruoyi.tc.service.ITcCarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 房产信息Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcCarServiceImpl implements ITcCarService {

    @Autowired
    private TcCarMapper tcCarMapper;

    /**
     * 查询房产信息
     *
     * @param id 房产信息ID
     * @return 房产信息
     */
    @Override
    public TcCar selectTcCarById(Long id) {
        return tcCarMapper.selectTcCarById(id);
    }

    /**
     * 查询房产信息列表
     *
     * @param tcCar 房产信息
     * @return 房产信息
     */
    @Override
    public List<TcCar> selectTcCarList(TcCar tcCar) {
        return tcCarMapper.selectTcCarList(tcCar);
    }

    /**
     * 新增房产信息
     *
     * @param tcCar 房产信息
     * @return 结果
     */
    @Override
    public int insertTcCar(TcCar tcCar) {
        tcCar.setCreateTime(DateUtils.getNowDate());
        return tcCarMapper.insertTcCar(tcCar);
    }

    /**
     * 修改房产信息
     *
     * @param tcCar 房产信息
     * @return 结果
     */
    @Override
    public int updateTcCar(TcCar tcCar) {
        return tcCarMapper.updateTcCar(tcCar);
    }

    /**
     * 批量删除房产信息
     *
     * @param ids 需要删除的房产信息ID
     * @return 结果
     */
    @Override
    public int deleteTcCarByIds(Long[] ids) {
        return tcCarMapper.deleteTcCarByIds(ids);
    }

    /**
     * 删除房产信息信息
     *
     * @param id 房产信息ID
     * @return 结果
     */
    @Override
    public int deleteTcCarById(Long id) {
        return tcCarMapper.deleteTcCarById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcCarMapper.countByMonth(customerId);
    }

}
