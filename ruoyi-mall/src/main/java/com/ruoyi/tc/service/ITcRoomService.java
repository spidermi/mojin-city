package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcRoom;

import java.util.List;

/**
 * 房产信息Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcRoomService {
    /**
     * 查询房产信息
     *
     * @param id 房产信息ID
     * @return 房产信息
     */
    public TcRoom selectTcRoomById(Long id);

    /**
     * 查询房产信息列表
     *
     * @param tcRoom 房产信息
     * @return 房产信息集合
     */
    public List<TcRoom> selectTcRoomList(TcRoom tcRoom);

    /**
     * 新增房产信息
     *
     * @param tcRoom 房产信息
     * @return 结果
     */
    public int insertTcRoom(TcRoom tcRoom);

    /**
     * 修改房产信息
     *
     * @param tcRoom 房产信息
     * @return 结果
     */
    public int updateTcRoom(TcRoom tcRoom);

    /**
     * 批量删除房产信息
     *
     * @param ids 需要删除的房产信息ID
     * @return 结果
     */
    public int deleteTcRoomByIds(Long[] ids);

    /**
     * 删除房产信息信息
     *
     * @param id 房产信息ID
     * @return 结果
     */
    public int deleteTcRoomById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
