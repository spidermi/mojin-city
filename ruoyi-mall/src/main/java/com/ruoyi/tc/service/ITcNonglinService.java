package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcNonglin;

import java.util.List;

/**
 * 农林牧渔Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcNonglinService {
    /**
     * 查询农林牧渔
     *
     * @param id 农林牧渔ID
     * @return 农林牧渔
     */
    public TcNonglin selectTcNonglinById(Long id);

    /**
     * 查询农林牧渔列表
     *
     * @param tcNonglin 农林牧渔
     * @return 农林牧渔集合
     */
    public List<TcNonglin> selectTcNonglinList(TcNonglin tcNonglin);

    /**
     * 新增农林牧渔
     *
     * @param tcNonglin 农林牧渔
     * @return 结果
     */
    public int insertTcNonglin(TcNonglin tcNonglin);

    /**
     * 修改农林牧渔
     *
     * @param tcNonglin 农林牧渔
     * @return 结果
     */
    public int updateTcNonglin(TcNonglin tcNonglin);

    /**
     * 批量删除农林牧渔
     *
     * @param ids 需要删除的农林牧渔ID
     * @return 结果
     */
    public int deleteTcNonglinByIds(Long[] ids);

    /**
     * 删除农林牧渔信息
     *
     * @param id 农林牧渔ID
     * @return 结果
     */
    public int deleteTcNonglinById(Long id);

    /**
     * 当月发布数
     * @param userId
     * @return
     */
    Long countByMonth(Long userId);
}
