package com.ruoyi.tc.service;

import com.ruoyi.tc.domain.TcMemberLevel;

import java.util.List;

/**
 * vip等级Service接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface ITcMemberLevelService {
    /**
     * 查询vip等级
     *
     * @param id vip等级ID
     * @return vip等级
     */
    public TcMemberLevel selectTcMemberLevelById(Long id);

    /**
     * 查询vip等级列表
     *
     * @param tcMemberLevel vip等级
     * @return vip等级集合
     */
    public List<TcMemberLevel> selectTcMemberLevelList(TcMemberLevel tcMemberLevel);

    /**
     * 新增vip等级
     *
     * @param tcMemberLevel vip等级
     * @return 结果
     */
    public int insertTcMemberLevel(TcMemberLevel tcMemberLevel);

    /**
     * 修改vip等级
     *
     * @param tcMemberLevel vip等级
     * @return 结果
     */
    public int updateTcMemberLevel(TcMemberLevel tcMemberLevel);

    /**
     * 批量删除vip等级
     *
     * @param ids 需要删除的vip等级ID
     * @return 结果
     */
    public int deleteTcMemberLevelByIds(Long[] ids);

    /**
     * 删除vip等级信息
     *
     * @param id vip等级ID
     * @return 结果
     */
    public int deleteTcMemberLevelById(Long id);
}
