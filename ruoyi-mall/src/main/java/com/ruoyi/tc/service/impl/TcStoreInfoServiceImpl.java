package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcStoreInfo;
import com.ruoyi.tc.mapper.TcStoreInfoMapper;
import com.ruoyi.tc.service.ITcStoreInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商家店铺Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcStoreInfoServiceImpl implements ITcStoreInfoService {

    @Autowired
    private TcStoreInfoMapper tcStoreInfoMapper;

    /**
     * 查询商家店铺
     *
     * @param id 商家店铺ID
     * @return 商家店铺
     */
    @Override
    public TcStoreInfo selectTcStoreInfoById(Long id) {
        return tcStoreInfoMapper.selectTcStoreInfoById(id);
    }

    /**
     * 查询商家店铺列表
     *
     * @param tcStoreInfo 商家店铺
     * @return 商家店铺
     */
    @Override
    public List<TcStoreInfo> selectTcStoreInfoList(TcStoreInfo tcStoreInfo) {
        return tcStoreInfoMapper.selectTcStoreInfoList(tcStoreInfo);
    }
    @Override
    public List<TcStoreInfo> selectTcStoreInfoListByDistinct(TcStoreInfo tcStoreInfo) {
        return tcStoreInfoMapper.selectTcStoreInfoListByDistinct(tcStoreInfo);
    }

    /**
     * 新增商家店铺
     *
     * @param tcStoreInfo 商家店铺
     * @return 结果
     */
    @Override
    public int insertTcStoreInfo(TcStoreInfo tcStoreInfo) {
        tcStoreInfo.setCreateTime(DateUtils.getNowDate());
        tcStoreInfo.setHit(0L);tcStoreInfo.setScope(0L);
        return tcStoreInfoMapper.insertTcStoreInfo(tcStoreInfo);
    }

    /**
     * 修改商家店铺
     *
     * @param tcStoreInfo 商家店铺
     * @return 结果
     */
    @Override
    public int updateTcStoreInfo(TcStoreInfo tcStoreInfo) {
        return tcStoreInfoMapper.updateTcStoreInfo(tcStoreInfo);
    }

    /**
     * 批量删除商家店铺
     *
     * @param ids 需要删除的商家店铺ID
     * @return 结果
     */
    @Override
    public int deleteTcStoreInfoByIds(Long[] ids) {
        return tcStoreInfoMapper.deleteTcStoreInfoByIds(ids);
    }

    /**
     * 删除商家店铺信息
     *
     * @param id 商家店铺ID
     * @return 结果
     */
    @Override
    public int deleteTcStoreInfoById(Long id) {
        return tcStoreInfoMapper.deleteTcStoreInfoById(id);
    }
}
