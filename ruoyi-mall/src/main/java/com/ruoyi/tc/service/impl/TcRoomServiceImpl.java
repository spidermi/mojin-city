package com.ruoyi.tc.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.tc.domain.TcRoom;
import com.ruoyi.tc.mapper.TcRoomMapper;
import com.ruoyi.tc.service.ITcRoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 房产信息Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Slf4j
@Service
public class TcRoomServiceImpl implements ITcRoomService {

    @Autowired
    private TcRoomMapper tcRoomMapper;

    /**
     * 查询房产信息
     *
     * @param id 房产信息ID
     * @return 房产信息
     */
    @Override
    public TcRoom selectTcRoomById(Long id) {
        return tcRoomMapper.selectTcRoomById(id);
    }

    /**
     * 查询房产信息列表
     *
     * @param tcRoom 房产信息
     * @return 房产信息
     */
    @Override
    public List<TcRoom> selectTcRoomList(TcRoom tcRoom) {
        return tcRoomMapper.selectTcRoomList(tcRoom);
    }

    /**
     * 新增房产信息
     *
     * @param tcRoom 房产信息
     * @return 结果
     */
    @Override
    public int insertTcRoom(TcRoom tcRoom) {
        tcRoom.setCreateTime(DateUtils.getNowDate());
        return tcRoomMapper.insertTcRoom(tcRoom);
    }

    /**
     * 修改房产信息
     *
     * @param tcRoom 房产信息
     * @return 结果
     */
    @Override
    public int updateTcRoom(TcRoom tcRoom) {
        return tcRoomMapper.updateTcRoom(tcRoom);
    }

    /**
     * 批量删除房产信息
     *
     * @param ids 需要删除的房产信息ID
     * @return 结果
     */
    @Override
    public int deleteTcRoomByIds(Long[] ids) {
        return tcRoomMapper.deleteTcRoomByIds(ids);
    }

    /**
     * 删除房产信息信息
     *
     * @param id 房产信息ID
     * @return 结果
     */
    @Override
    public int deleteTcRoomById(Long id) {
        return tcRoomMapper.deleteTcRoomById(id);
    }
    @Override
    public  Long countByMonth(Long customerId){
        return tcRoomMapper.countByMonth(customerId);
    }
}
