package com.ruoyi.tc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;


import com.ruoyi.util.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ruoyi.tc.mapper.TcCollectMapper;
import com.ruoyi.tc.domain.TcCollect;
import com.ruoyi.tc.service.ITcCollectService;

/**
 * 商品关注Service业务层处理
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@Slf4j
@Service
public class TcCollectServiceImpl implements ITcCollectService {

    @Autowired
    private TcCollectMapper tcCollectMapper;

    /**
     * 查询商品关注
     *
     * @param id 商品关注ID
     * @return 商品关注
     */
    @Override
    public TcCollect selectTcCollectById(Long id) {
        return tcCollectMapper.selectTcCollectById(id);
    }

    /**
     * 查询商品关注列表
     *
     * @param tcCollect 商品关注
     * @return 商品关注
     */
    @Override
    public List<TcCollect> selectTcCollectList(TcCollect tcCollect) {
        return tcCollectMapper.selectTcCollectList(tcCollect);
    }

    /**
     * 新增商品关注
     *
     * @param tcCollect 商品关注
     * @return 结果
     */
    @Override
    public int insertTcCollect(TcCollect tcCollect) {
                                                                                                                    tcCollect.setCreateTime(DateUtils.getNowDate());
                                                                    return tcCollectMapper.insertTcCollect(tcCollect);
    }

    /**
     * 修改商品关注
     *
     * @param tcCollect 商品关注
     * @return 结果
     */
    @Override
    public int updateTcCollect(TcCollect tcCollect) {
                                                                                                                                                            return tcCollectMapper.updateTcCollect(tcCollect);
    }

    /**
     * 批量删除商品关注
     *
     * @param ids 需要删除的商品关注ID
     * @return 结果
     */
    @Override
    public int deleteTcCollectByIds(Long[] ids) {
        return tcCollectMapper.deleteTcCollectByIds(ids);
    }

    /**
     * 删除商品关注信息
     *
     * @param id 商品关注ID
     * @return 结果
     */
    @Override
    public int deleteTcCollectById(Long id) {
        return tcCollectMapper.deleteTcCollectById(id);
    }
    @Override
    public PageHelper<TcCollect> queryAttentionsForCustomerCentre(PageHelper<TcCollect> pageHelper, long customerId,Integer type) {
        Map<String, Object> params = new HashMap<>();
        params.put("customerId", customerId);
        params.put("type", type);
        return pageHelper.setListDates(tcCollectMapper.queryAttentions(pageHelper.getQueryParams(params, tcCollectMapper.count(params))));
    }


}
