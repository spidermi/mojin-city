package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 求购对象 tc_qiugou
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcQiugou extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private TcMember member;
    /**
     * $column.columnComment
     */
    private Long id;
    private Long topId;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String name;

    /**
     * 高预算
     */
    @Excel(name = "高预算")
    private Long highPrice;

    /**
     * 低预算
     */
    @Excel(name = "低预算")
    private Long lowPrice;

    /**
     * 1房屋求购 2房屋求租 3车辆求购4 物品求购5 寻人寻物 6 其他
     */
    @Excel(name = "1房屋求购 2房屋求租 3车辆求购4 物品求购5 寻人寻物 6 其他")
    private String type;

    /**
     * 轮播图
     */
    @Excel(name = "轮播图")
    private String pics;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String contactPhone;

    /**
     * 置顶状态
     */
    @Excel(name = "置顶状态")
    private String isZhiding;

    /**
     * 置顶到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "置顶到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhidingTime;

    /**
     * 1已上线 2审核中 3已下线4 已删除5 待发布
     */
    @Excel(name = "1已上线 2审核中 3已下线4 已删除5 待发布")
    private String status;

    private String pic;
    private String []picList;
    public String getPic() {
        if (pics!=null){
            return pics.split(",")[0];
        }
        return null;
    }
    public String[] getPicList() {
        if (pics!=null){
            return pics.split(",");
        }
        return null;
    }
}
