package com.ruoyi.tc.domain;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.tc.vo.TcMemberLevelVo;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * vip等级对象 tc_member_level
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcMemberLevel extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date closeTime;
    private Long publishedCount; // 已发布数量

// [{"month": 1, "price": "100.00"}, {"month": 3, "price": "300.00"}, {"month": 12, "price": "1,200.00"}]
    private String selpay;
    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 会员价/月
     */
    @Excel(name = "会员价/月")
    private BigDecimal price;

    /**
     * 等级名称
     */
    @Excel(name = "等级名称")
    private String name;

    /**
     * 招聘免费发布数/月
     */
    @Excel(name = "招聘免费发布数/月")
    private Long jobCount;

    /**
     * 房源免费发布数/月
     */
    @Excel(name = "房源免费发布数/月")
    private Long roomCount;

    /**
     * 车辆免费发布数/月
     */
    @Excel(name = "车辆免费发布数/月")
    private Long carCount;

    /**
     * 物品免费发布数/月
     */
    @Excel(name = "物品免费发布数/月")
    private Long goodsCount;

    /**
     * 农林免费发布数/月
     */
    @Excel(name = "农林免费发布数/月")
    private Long nongCount;

    /**
     * 生意免费发布数/月
     */
    @Excel(name = "生意免费发布数/月")
    private Long businessCount;

    /**
     * 推广免费发布数/月
     */
    @Excel(name = "推广免费发布数/月")
    private Long smsCount;

    /**
     * 打听免费发布数/月
     */
    @Excel(name = "打听免费发布数/月")
    private Long speakCount;

    /**
     * 车找人免费发布数/月:
     */
    @Excel(name = "车找人免费发布数/月:")
    private Long carToPeopleCount;

    /**
     * 人找车免费发布数/月
     */
    @Excel(name = "人找车免费发布数/月")
    private Long peopleToCarCount;

    /**
     * 超过免费发布数价格/条
     */
    @Excel(name = "超过免费发布数价格/条")
    private Long moreCount;

    /**
     * 置顶折扣/月(1-100/100)
     */
    @Excel(name = "置顶折扣/月(1-100/100)")
    private Double topCount;
    private List<TcMemberLevelVo> rules;
    public List<TcMemberLevelVo> getRules() {
        if (selpay!=null){
           return JSONArray.parseArray(selpay,TcMemberLevelVo.class);
        }
        return null;
    }

}
