package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 房产信息对象 tc_car
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcCar extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Long topId;
    private TcMember member;
    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String name;

    /**
     * 上牌时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上牌时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date shangpaiTime;
    private Long shangpaiTimes;
    /**
     * 里程
     */
    @Excel(name = "里程")
    private Long licheng;

    /**
     * 总价
     */
    @Excel(name = "总价")
    private Long zongjia;

    /**
     * 标签
     */
    @Excel(name = "标签")
    private String tagIds;
    private Long[] tagIdsIds;
    /**
     * 轮播图
     */
    @Excel(name = "轮播图")
    private String pics;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String contactPhone;

    /**
     * 置顶状态
     */
    @Excel(name = "置顶状态")
    private String isZhiding;

    /**
     * 置顶到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "置顶到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhidingTime;

    /**
     * 1已上线 2审核中 3已下线4 已删除5 待发布
     */
    @Excel(name = "1已上线 2审核中 3已下线4 已删除5 待发布")
    private String status;

    private String pic;
    private String []picList;
    public String getPic() {
        if (pics!=null){
            return pics.split(",")[0];
        }
        return null;
    }
    public String[] getPicList() {
        if (pics!=null){
            return pics.split(",");
        }
        return null;
    }
    public String[] getTagIdsIds() {
        if (tagIds!=null){
            return tagIds.split(",");
        }
        return null;
    }

}
