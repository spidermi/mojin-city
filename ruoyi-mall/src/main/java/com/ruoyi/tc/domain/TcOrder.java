package com.ruoyi.tc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 订单对象 tc_order
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@Setter
@Getter
public class TcOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long customerId;

    /**
     * 支付金额
     */
    @Excel(name = "支付金额")
    private BigDecimal payAmount=BigDecimal.ZERO;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private String status;

    /**
     * 外部订单号
     */
    @Excel(name = "外部订单号")
    private String outTradeNo;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String title;

    /**
     * 类别
     */
    @Excel(name = "类别")
    private String type;

    /**
     * 信息发布费
     */
    @Excel(name = "信息发布费")
    private double publishAmout;

    /**
     * 置顶费
     */
    @Excel(name = "置顶费")
    private BigDecimal topAmount;

    /**
     * 置顶天数
     */
    @Excel(name = "置顶天数")
    private Long topDay;

    /**
     * 订单号
     */
    @Excel(name = "订单号")
    private String orderNo;

    /**
     * 信息折扣
     */
    @Excel(name = "信息折扣")
    private double publishSmsAmout;

    /**
     * 当前折扣
     */
    @Excel(name = "当前折扣")
    private Long memberLevel;

    private String openid;
    private String ip;
}
