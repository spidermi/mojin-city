package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 人才简历对象 tc_user_card
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcUserCard extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private TcMember member;
    private TcJobClass jobcategory;
    private Long topId;
    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String name;

    /**
     * 工作经验
     */
    @Excel(name = "工作经验")
    private String jobExpisre;

    /**
     * 学历要求
     */
    @Excel(name = "学历要求")
    private String xueli;

    /**
     * 职位分类代码
     */
    @Excel(name = "职位分类代码")
    private String jobClassId;

    /**
     * 年龄
     */
    @Excel(name = "年龄")
    private Long age;

    /**
     * 性别
     */
    @Excel(name = "性别")
    private String sex;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String contactPhone;

    /**
     * 置顶状态
     */
    @Excel(name = "置顶状态")
    private String isZhiding;

    /**
     * 置顶到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "置顶到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhidingTime;

    /**
     * 1已上线 2审核中 3已下线4 已删除5 待发布
     */
    @Excel(name = "1展示 3隐藏")
    private String status;


}
