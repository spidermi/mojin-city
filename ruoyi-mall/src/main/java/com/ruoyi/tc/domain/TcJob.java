package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 招聘信息对象 tc_job
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcJob extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private TcJobClass jobcategory;
    private String[] salaryLevelIds;
    private TcMember member;
    /**
     * $column.columnComment
     */
    private Long id;

    private Long topId;


    /**
     * 标题
     */
    @Excel(name = "标题")
    private String name;

    /**
     * 1面议
     */
    @Excel(name = "1面议")
    private String isMianyi;

    /**
     * 最低薪资
     */
    @Excel(name = "最低薪资")
    private BigDecimal lowSalary;

    /**
     * 最高薪资
     */
    @Excel(name = "最高薪资")
    private BigDecimal highSalary;

    /**
     * 职位分类代码
     */
    @Excel(name = "职位分类代码")
    private String jobClassId;

    /**
     * 工作经验
     */
    @Excel(name = "工作经验")
    private String jobExpisre;

    /**
     * 学历要求
     */
    @Excel(name = "学历要求")
    private String xueli;

    /**
     * 福利待遇
     */
    @Excel(name = "福利待遇")
    private String salaryLevel;

    /**
     * 经度 lng
     */
    @Excel(name = "经度")
    private Long lin;

    /**
     * 纬度 lat
     */
    @Excel(name = "纬度")
    private Long lon;

    /**
     * 工作地址
     */
    @Excel(name = "工作地址")
    private String address;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String contactPhone;

    /**
     * 置顶状态
     */
    @Excel(name = "置顶状态")
    private String isZhiding;

    /**
     * 置顶到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "置顶到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhidingTime;

    /**
     * 1已上线 2审核中 3已下线4 已删除5 待发布
     */
    @Excel(name = "1已上线 2审核中 3已下线4 已删除5 待发布")
    private String status;


}
