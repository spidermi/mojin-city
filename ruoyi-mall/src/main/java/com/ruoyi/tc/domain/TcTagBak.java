package com.ruoyi.tc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 标签对象 tc_tag
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcTagBak extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 分类
     */
    @Excel(name = "分类")
    private String type;

    /**
     * 分类
     */
    @Excel(name = "分类")
    private String house_rent_label;

    private String type_text;


}
