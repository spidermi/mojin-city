package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * 商家店铺对象 tc_store_info
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcStoreInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private List<TcStroeComment> stroeCommentList;
    private TcNav category;

    private TcNav pCategory;

    private Long distance;
    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date closeTime;

    /**
     * 营业天数
     */
    @Excel(name = "营业天数")
    private String yingye;

    /**
     * 营业时间
     */
    @Excel(name = "营业时间")
    private String yingyeTime;

    /**
     * 浏览次数
     */
    @Excel(name = "浏览次数")
    private Long hit;

    /**
     * 营业天数
     */
    @Excel(name = "分类")
    private Long classId;

    /**
     * 经度
     */
    @Excel(name = "经度")
    private String lin;

    /**
     * 纬度
     */
    @Excel(name = "纬度")
    private String lon;

    /**
     * 店铺地址
     */
    @Excel(name = "店铺地址")
    private String address;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String contactPhone;

    /**
     * 置顶状态
     */
    @Excel(name = "置顶状态")
    private String isZhiding;

    /**
     * 置顶到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "置顶到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhidingTime;

    /**
     * 1已上线 2审核中 3已下线4 已删除5 待发布
     */
    @Excel(name = "1已上线 2审核中 3已下线4 已删除5 待发布")
    private String status;

    /**
     * logo
     */
    @Excel(name = "logo")
    private String pic;

    /**
     * 营业执照
     */
    @Excel(name = "营业执照")
    private String zhizhao;

    /**
     * 海报
     */
    @Excel(name = "海报")
    private String haibao;

    /**
     * 轮播图
     */
    @Excel(name = "轮播图")
    private String pics;

    /**
     * 评分
     */
    @Excel(name = "评分")
    private Long scope;

    private String [] coverList;

    public String[] getCoverList() {
        if (pic!=null){
            return pic.split(",");
        }
        return null;
    }
    private String [] cardList;

    public String[] getCardList() {
        if (zhizhao!=null){
            return zhizhao.split(",");
        }
        return null;
    }
    private String [] picList;

    public String[] getPicList() {
        if (pics!=null){
            return pics.split(",");
        }
        return null;
    }
}
