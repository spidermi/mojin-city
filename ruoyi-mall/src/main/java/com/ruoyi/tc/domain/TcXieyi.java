package com.ruoyi.tc.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 标签对象 tc_xieyi
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcXieyi extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /**
     * 轮播位置：1首页 2 商家页
     */
    @Excel(name = "轮播位置：1 帮助文档 2 商家页")
    private Long type;


}
