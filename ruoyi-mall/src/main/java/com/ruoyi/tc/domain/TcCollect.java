package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 商品关注对象 tc_collect
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@Setter
@Getter
public class TcCollect extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 会员id
     */
    @Excel(name = "会员id")
    private Long customerId;

    /**
     * 商品id
     */
    @Excel(name = "商品id")
    private Long objId;

    /**
     * 删除标记 0 未删除 1 删除 默认0
     */
    private String delFlag;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    /**
     * 1 房屋 2 农林牧渔 3 物品交易 4推广 5 求购 6求职 7找人才 8转让 9商家店铺 10 车
     */

    private String type;

    /**
     * $column.columnComment
     */
    @Excel(name = "删除时间")
    private String name;

    /**
     * $column.columnComment
     */
    @Excel(name = "删除时间")
    private String pic;


}
