package com.ruoyi.tc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 房产信息对象 tc_room
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@Setter
@Getter
public class TcRoom extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private TcMember member;
    /**
     * $column.columnComment
     */
    private Long id;
    private Long topId;
    /**
     * 标题
     */
    @Excel(name = "标题")
    private String name;

    /**
     * 楼层
     */
    @Excel(name = "楼层")
    private Long loucheng;

    /**
     * 装修
     */
    @Excel(name = "装修")
    private String zhuangxiu;
        // 1出租 2出售
    private String userType;
    /**
     * 配置
     */
    @Excel(name = "配置")
    private String configIds;

    private String[] configIdsIds;

    /**
     * 户型
     */
    @Excel(name = "户型")
    private String huxing;

    /**
     * 标签
     */
    @Excel(name = "标签")
    private String tagIds;
    private String[] tagIdsIds;
    /**
     * 朝向
     */
    @Excel(name = "朝向")
    private String chaoxiang;

    /**
     * 房源类型
     */
    @Excel(name = "房源类型")
    private String rootType;

    /**
     * 面积
     */
    @Excel(name = "面积")
    private Long mianji;

    /**
     * 租金
     */
    @Excel(name = "租金")
    private Long zujin;

    /**
     * 经度
     */
    @Excel(name = "经度")
    private Long lin;

    /**
     * 纬度
     */
    @Excel(name = "纬度")
    private Long lon;

    /**
     * 工作地址
     */
    @Excel(name = "工作地址")
    private String address;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String contactPhone;

    /**
     * 置顶状态
     */
    @Excel(name = "置顶状态")
    private String isZhiding;

    /**
     * 置顶到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "置顶到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhidingTime;

    /**
     * 1已上线 2审核中 3已下线4 已删除5 待发布
     */
    @Excel(name = "1已上线 2审核中 3已下线4 已删除5 待发布")
    private String status;

    /**
     * $column.columnComment
     */
    @Excel(name = "1已上线 2审核中 3已下线4 已删除5 待发布")
    private String pics;

    private String pic;
    private String[] picList;
    public String getPic() {
        if (pics!=null){
            return pics.split(",")[0];
        }
        return null;
    }
    public String[] getPicList() {
        if (pics!=null){
            return pics.split(",");
        }
        return null;
    }


    public String[] getConfigIdsIds() {
        if (configIds!=null){
            return configIds.split(",");
        }
        return null;
    }

    public String[] getTagIdsIds() {
        if (tagIds!=null){
            return tagIds.split(",");
        }
        return null;
    }

}
