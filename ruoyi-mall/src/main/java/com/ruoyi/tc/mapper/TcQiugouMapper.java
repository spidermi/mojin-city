package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcQiugou;

import java.util.List;

/**
 * 求购Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcQiugouMapper {
    /**
     * 查询求购
     *
     * @param id 求购ID
     * @return 求购
     */
    public TcQiugou selectTcQiugouById(Long id);


    /**
     * 批量查询求购
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcQiugou> selectTcQiugouByIds(Long[] ids);

    /**
     * 查询求购列表
     *
     * @param tcQiugou 求购
     * @return 求购集合
     */
    public List<TcQiugou> selectTcQiugouList(TcQiugou tcQiugou);

    /**
     * 新增求购
     *
     * @param tcQiugou 求购
     * @return 结果
     */
    public int insertTcQiugou(TcQiugou tcQiugou);

    /**
     * 修改求购
     *
     * @param tcQiugou 求购
     * @return 结果
     */
    public int updateTcQiugou(TcQiugou tcQiugou);

    /**
     * 删除求购
     *
     * @param id 求购ID
     * @return 结果
     */
    public int deleteTcQiugouById(Long id);

    /**
     * 批量删除求购
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcQiugouByIds(Long[] ids);

    Long countByMonth(long customerId);
}
