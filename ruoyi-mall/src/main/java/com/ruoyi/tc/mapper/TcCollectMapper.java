package com.ruoyi.tc.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.tc.domain.TcCollect;

/**
 * 商品关注Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-23
 */
public interface TcCollectMapper {
    /**
     * 查询商品关注
     *
     * @param id 商品关注ID
     * @return 商品关注
     */
    public TcCollect selectTcCollectById(Long id);


      /**
         * 批量查询商品关注
         *
         * @param ids 需要查询的数据ID
         * @return 结果
         */
      public List<TcCollect>  selectTcCollectByIds(Long[] ids);

    /**
     * 查询商品关注列表
     *
     * @param tcCollect 商品关注
     * @return 商品关注集合
     */
    public List<TcCollect> selectTcCollectList(TcCollect tcCollect);

    /**
     * 新增商品关注
     *
     * @param tcCollect 商品关注
     * @return 结果
     */
    public int insertTcCollect(TcCollect tcCollect);

    /**
     * 修改商品关注
     *
     * @param tcCollect 商品关注
     * @return 结果
     */
    public int updateTcCollect(TcCollect tcCollect);

    /**
     * 删除商品关注
     *
     * @param id 商品关注ID
     * @return 结果
     */
    public int deleteTcCollectById(Long id);

    /**
     * 批量删除商品关注
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcCollectByIds(Long[] ids);

    /**
     * 查询商品关注总数
     *
     * @param params 查询参数
     * @return 返回商品关注总数
     */

    int count(Map<String, Object> params);

    /**
     * 查询商品关注
     *
     * @param params 查询参数
     * @return 返回商品关注
     */

    List<TcCollect> queryAttentions(Map<String, Object> params);
}
