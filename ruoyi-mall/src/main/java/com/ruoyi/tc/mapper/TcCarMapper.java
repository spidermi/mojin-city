package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcCar;

import java.util.List;

/**
 * 房产信息Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcCarMapper {
    /**
     * 查询房产信息
     *
     * @param id 房产信息ID
     * @return 房产信息
     */
    public TcCar selectTcCarById(Long id);


    /**
     * 批量查询房产信息
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcCar> selectTcCarByIds(Long[] ids);

    /**
     * 查询房产信息列表
     *
     * @param tcCar 房产信息
     * @return 房产信息集合
     */
    public List<TcCar> selectTcCarList(TcCar tcCar);

    /**
     * 新增房产信息
     *
     * @param tcCar 房产信息
     * @return 结果
     */
    public int insertTcCar(TcCar tcCar);

    /**
     * 修改房产信息
     *
     * @param tcCar 房产信息
     * @return 结果
     */
    public int updateTcCar(TcCar tcCar);

    /**
     * 删除房产信息
     *
     * @param id 房产信息ID
     * @return 结果
     */
    public int deleteTcCarById(Long id);

    /**
     * 批量删除房产信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcCarByIds(Long[] ids);

    Long countByMonth(long customerId);
}
