package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcNonglin;

import java.util.List;

/**
 * 农林牧渔Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcNonglinMapper {
    /**
     * 查询农林牧渔
     *
     * @param id 农林牧渔ID
     * @return 农林牧渔
     */
    public TcNonglin selectTcNonglinById(Long id);


    /**
     * 批量查询农林牧渔
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcNonglin> selectTcNonglinByIds(Long[] ids);

    /**
     * 查询农林牧渔列表
     *
     * @param tcNonglin 农林牧渔
     * @return 农林牧渔集合
     */
    public List<TcNonglin> selectTcNonglinList(TcNonglin tcNonglin);

    /**
     * 新增农林牧渔
     *
     * @param tcNonglin 农林牧渔
     * @return 结果
     */
    public int insertTcNonglin(TcNonglin tcNonglin);

    /**
     * 修改农林牧渔
     *
     * @param tcNonglin 农林牧渔
     * @return 结果
     */
    public int updateTcNonglin(TcNonglin tcNonglin);

    /**
     * 删除农林牧渔
     *
     * @param id 农林牧渔ID
     * @return 结果
     */
    public int deleteTcNonglinById(Long id);

    /**
     * 批量删除农林牧渔
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcNonglinByIds(Long[] ids);

    Long countByMonth(long customerId);
}
