package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcJobClass;

import java.util.List;

/**
 * 职位分类Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcJobClassMapper {
    /**
     * 查询职位分类
     *
     * @param id 职位分类ID
     * @return 职位分类
     */
    public TcJobClass selectTcJobClassById(Long id);


    /**
     * 批量查询职位分类
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcJobClass> selectTcJobClassByIds(Long[] ids);

    /**
     * 查询职位分类列表
     *
     * @param tcJobClass 职位分类
     * @return 职位分类集合
     */
    public List<TcJobClass> selectTcJobClassList(TcJobClass tcJobClass);

    /**
     * 新增职位分类
     *
     * @param tcJobClass 职位分类
     * @return 结果
     */
    public int insertTcJobClass(TcJobClass tcJobClass);

    /**
     * 修改职位分类
     *
     * @param tcJobClass 职位分类
     * @return 结果
     */
    public int updateTcJobClass(TcJobClass tcJobClass);

    /**
     * 删除职位分类
     *
     * @param id 职位分类ID
     * @return 结果
     */
    public int deleteTcJobClassById(Long id);

    /**
     * 批量删除职位分类
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcJobClassByIds(Long[] ids);
}
