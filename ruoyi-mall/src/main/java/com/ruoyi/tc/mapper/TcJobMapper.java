package com.ruoyi.tc.mapper;

import com.ruoyi.tc.domain.TcJob;

import java.util.List;

/**
 * 招聘信息Mapper接口
 *
 * @author é­éåå
 * @date 2022-01-21
 */
public interface TcJobMapper {
    /**
     * 查询招聘信息
     *
     * @param id 招聘信息ID
     * @return 招聘信息
     */
    public TcJob selectTcJobById(Long id);


    /**
     * 批量查询招聘信息
     *
     * @param ids 需要查询的数据ID
     * @return 结果
     */
    public List<TcJob> selectTcJobByIds(Long[] ids);

    /**
     * 查询招聘信息列表
     *
     * @param tcJob 招聘信息
     * @return 招聘信息集合
     */
    public List<TcJob> selectTcJobList(TcJob tcJob);

    /**
     * 新增招聘信息
     *
     * @param tcJob 招聘信息
     * @return 结果
     */
    public int insertTcJob(TcJob tcJob);

    /**
     * 修改招聘信息
     *
     * @param tcJob 招聘信息
     * @return 结果
     */
    public int updateTcJob(TcJob tcJob);

    /**
     * 删除招聘信息
     *
     * @param id 招聘信息ID
     * @return 结果
     */
    public int deleteTcJobById(Long id);

    /**
     * 批量删除招聘信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTcJobByIds(Long[] ids);

    Long countByMonth(long customerId);
}
