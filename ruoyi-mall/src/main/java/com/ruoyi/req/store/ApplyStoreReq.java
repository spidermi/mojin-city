package com.ruoyi.req.store;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 申请门店
 * @Author: Jayvin Leung
 * @Date: 2019/9/10 1:09
 */
@Getter
@Setter
public class ApplyStoreReq implements Serializable {
    public static final String URI = "/store/apply.do";
    private String imgUrls;
    private String description;
    private String storeNo;
    private String title;
    private String provinceName;
    private String cityName;
    private String areaName;
    private String address;
    private String showAddress;
    private String houseNumber;
    private String lon;
    private String lat;
    private String phone;
    private String businessLicense;
    private Long companyId;

    @Override
    public String toString() {
        return "ApplyStoreReq{" +
                "imgUrls='" + imgUrls + '\'' +
                ", description='" + description + '\'' +
                ", storeNo='" + storeNo + '\'' +
                ", title='" + title + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", areaName='" + areaName + '\'' +
                ", address='" + address + '\'' +
                ", showAddress='" + showAddress + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", lon='" + lon + '\'' +
                ", lat='" + lat + '\'' +
                ", phone='" + phone + '\'' +
                ", businessLicense='" + businessLicense + '\'' +
                ", companyId=" + companyId +
                '}';
    }
}
