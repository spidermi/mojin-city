package com.ruoyi.req.store;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 门店财务报表
 * @Author: Jayvin Leung
 * @Date: 2019/9/11 4:13
 */
@Getter
@Setter
public class StoreFinanceReq implements Serializable {
    public static final String URI = "/store/getFinanceInfo.do";
    private Long storeId;
    private int type;
    private Long dayTime;

    @Override
    public String toString() {
        return "StoreFinanceReq{" +
                "storeId=" + storeId +
                ", type=" + type +
                ", dayTime=" + dayTime +
                '}';
    }
}
