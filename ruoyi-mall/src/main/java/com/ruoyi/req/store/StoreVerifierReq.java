package com.ruoyi.req.store;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 门店福气核销明细
 * @Author: Jayvin Leung
 * @Date: 2019/9/11 4:13
 */
@Getter
@Setter
public class StoreVerifierReq implements Serializable {
    public static final String URI = "/store/getVerifierInfo.do";
    private Long storeId;
    private int type;
    private Long dayTime;

    @Override
    public String toString() {
        return "StoreVerifierReq{" +
                "storeId=" + storeId +
                ", type=" + type +
                ", dayTime=" + dayTime +
                '}';
    }
}
