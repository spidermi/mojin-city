package com.ruoyi.req.user.account;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 小程序授权登录
 * @Author: Jayvin Leung
 * @Date: 2019/9/3 16:17
 */
@Getter
@Setter
public class LoginReq implements Serializable {
    public static final String URI = "/account/login.do";// 用户登录
    private String jsCode; // jsCode
    private String rawData; // 不包括敏感信息的原始数据字符串，用于计算签名。
    private String signature; // 使用 sha1( rawData + sessionkey ) 得到字符串，用于校验用户信息。
    private String encryptedData; // 经过加密的数据字符串
    private String iv; // 加密算法的初始向量，详细见加密数据解密算法
    private String promoCode; // 邀请码
    private Long storeId; // 当前门店id
    private String lon;
    private String lat;

    @Override
    public String toString() {
        return "LoginReq{" +
                "jsCode='" + jsCode + '\'' +
                ", rawData='" + rawData + '\'' +
                ", signature='" + signature + '\'' +
                ", encryptedData='" + encryptedData + '\'' +
                ", iv='" + iv + '\'' +
                ", promoCode='" + promoCode + '\'' +
                ", storeId=" + storeId +
                ", lon='" + lon + '\'' +
                ", lat='" + lat + '\'' +
                '}';
    }
}
