package com.ruoyi.req.user.withdraw;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 发起提现
 * @Author: Jayvin Leung
 * @Date: 2019/9/9 23:20
 */
@Getter
@Setter
public class WithdrawReq implements Serializable {
    public static final String URI = "/withdraw/apply.do";
    private BigDecimal money = new BigDecimal(0);

    @Override
    public String toString() {
        return "WithdrawReq{" +
                "money=" + money +
                '}';
    }
}
