package com.ruoyi.req.brand;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 品牌详情
 * @Author: Jayvin Leung
 * @Date: 2019/9/3 2:25
 */
@Getter
@Setter
public class BrandDetailReq implements Serializable {
    public static final String URI = "/brand/getBrandDetail.do";
    private Long brandId;

    @Override
    public String toString() {
        return "BrandDetailReq{" +
                "brandId=" + brandId +
                '}';
    }
}
