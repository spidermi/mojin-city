package com.ruoyi.req.brand;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 品牌
 * @Author: Jayvin Leung
 * @Date: 2019/9/2 14:04
 */
@Getter
@Setter
public class BrandListReq extends PageReq implements Serializable {
    public static final String URI = "/brand/getBrandList.do";
    private int platformFlag = 2;
    private Long storeId;

    @Override
    public String toString() {
        return "BrandListReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
