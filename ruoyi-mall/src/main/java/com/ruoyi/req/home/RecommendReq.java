package com.ruoyi.req.home;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 推荐榜单
 * @Author: Jayvin Leung
 * @Date: 2019/9/5 11:36
 */
@Getter
@Setter
public class RecommendReq extends PageReq implements Serializable {
    public static final String URI = "/home/getRecommend.do";
    private int platformFlag = 2; // 平台标识
    private Long storeId;
    private Long classifyId; // 分类id

    @Override
    public String toString() {
        return "RecommendReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                ", classifyId=" + classifyId +
                '}';
    }
}
