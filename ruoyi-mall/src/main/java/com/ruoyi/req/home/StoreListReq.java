package com.ruoyi.req.home;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 门店列表
 * @Author: Jayvin Leung
 * @Date: 2019/9/3 12:08
 */
@Getter
@Setter
public class StoreListReq extends PageReq implements Serializable {
    public static final String URI = "/home/getStoreList.do";
    private String lon; // 经度
    private String lat; // 纬度

    @Override
    public String toString() {
        return "StoreListReq{" +
                "lon='" + lon + '\'' +
                ", lat='" + lat + '\'' +
                '}';
    }
}
