package com.ruoyi.req.home;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 秒杀（限时购）
 * @Author: Jayvin Leung
 * @Date: 2019/9/2 14:04
 */
@Getter
@Setter
public class SecKillReq extends PageReq implements Serializable {
    public static final String URI = "/home/getSecKill.do";
    private int platformFlag = 2;
    private Long storeId;

    @Override
    public String toString() {
        return "SecKillReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
