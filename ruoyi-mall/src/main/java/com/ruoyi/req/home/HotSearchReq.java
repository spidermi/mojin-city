package com.ruoyi.req.home;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 热搜词列表
 * @Author: Jayvin Leung
 * @Date: 2019/9/5 11:39
 */
@Getter
@Setter
public class HotSearchReq implements Serializable {
    public static final String URI = "/home/getHotSearchList.do";
    private int platformFlag = 2;
    private Long storeId;

    @Override
    public String toString() {
        return "HotSearchReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
