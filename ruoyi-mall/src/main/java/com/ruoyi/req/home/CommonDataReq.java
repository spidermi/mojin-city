package com.ruoyi.req.home;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 热搜词
 * @Author: Jayvin Leung
 * @Date: 2019/9/2 11:21
 */
@Getter
@Setter
public class CommonDataReq extends PageReq implements Serializable {
    public static final String URI = "/home/getCommonData.do";
    private int platformFlag = 2;
    private Long storeId;

    @Override
    public String toString() {
        return "CommonDataReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
