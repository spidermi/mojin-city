package com.ruoyi.req.sign;

import lombok.Data;

@Data
public class SignQrCodeStoreReq {
    private Long userId;
    private Integer width;
    private Integer height;
}
