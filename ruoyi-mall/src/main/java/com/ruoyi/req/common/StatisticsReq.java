package com.ruoyi.req.common;

import lombok.Data;

@Data
public class StatisticsReq {
    Long distributionStartTime;
    Long distributionEndTime;
    Long createStartTime;
    Long createEndTime;
    Integer type;
    Integer store;
    String isStore;

}
