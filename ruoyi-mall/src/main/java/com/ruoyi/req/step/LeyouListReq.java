package com.ruoyi.req.step;

import com.ruoyi.req.base.PageReq;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 我的乐友列表
 * @Author: Jayvin Leung
 * @Date: 2019/9/8 21:14
 */
@Getter
@Setter
public class LeyouListReq extends PageReq implements Serializable {
    public static final String URI = "/step/getLeyouList.do";
    private int level = 1;
    private String memberId;

    @Override
    public String toString() {
        return "LeyouListReq{" +
                "level=" + level +
                '}';
    }
}
