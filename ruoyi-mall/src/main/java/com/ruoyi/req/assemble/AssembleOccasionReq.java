package com.ruoyi.req.assemble;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 拼团应景图
 * @Author: Jayvin Leung
 * @Date: 2019/9/4 14:55
 */
@Getter
@Setter
public class AssembleOccasionReq implements Serializable {
    public static final String URI = "/assemble/getOccasion.do";
    private int platformFlag = 2;
    private Long storeId;

    @Override
    public String toString() {
        return "AssembleOccasionReq{" +
                "platformFlag=" + platformFlag +
                ", storeId=" + storeId +
                '}';
    }
}
