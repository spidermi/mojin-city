package com.ruoyi.req.assemble;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Description: 拼团详情
 * @Author: Jayvin Leung
 * @Date: 2019/9/5 1:12
 */
@Getter
@Setter
public class UserAssembleDetailReq implements Serializable {
    public static final String URI = "/assemble/getUserAssembleDetail.do";
    private Long userAssembleId;

    @Override
    public String toString() {
        return "UserAssembleDetailReq{" +
                "userAssembleId=" + userAssembleId +
                '}';
    }
}
