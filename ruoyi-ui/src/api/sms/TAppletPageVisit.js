import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTAppletPageVisit(query) {
    return request({
        url: '/sms/TAppletPageVisit/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getTAppletPageVisit(id) {
    return request({
        url: '/sms/TAppletPageVisit/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addTAppletPageVisit(data) {
    return request({
        url: '/sms/TAppletPageVisit',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateTAppletPageVisit(data) {
    return request({
        url: '/sms/TAppletPageVisit',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delTAppletPageVisit(id) {
    return request({
        url: '/sms/TAppletPageVisit/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportTAppletPageVisit(query) {
    return request({
        url: '/sms/TAppletPageVisit/export',
        method: 'get',
        params: query
    })
}