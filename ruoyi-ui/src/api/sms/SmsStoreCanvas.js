import request from '@/utils/request'

// 查询画布信息列表
export function listSmsStoreCanvas(query) {
    return request({
        url: '/sms/SmsStoreCanvas/list',
        method: 'get',
        params: query
    })
}

// 查询画布信息详细
export function getSmsStoreCanvas(canvasId) {
    return request({
        url: '/sms/SmsStoreCanvas/' + canvasId,
        method: 'get'
    })
}

// 新增画布信息
export function addSmsStoreCanvas(data) {
    return request({
        url: '/sms/SmsStoreCanvas',
        method: 'post',
        data: data
    })
}

// 修改画布信息
export function updateSmsStoreCanvas(data) {
    return request({
        url: '/sms/SmsStoreCanvas',
        method: 'put',
        data: data
    })
}

// 删除画布信息
export function delSmsStoreCanvas(canvasId) {
    return request({
        url: '/sms/SmsStoreCanvas/' + canvasId,
        method: 'delete'
    })
}

// 导出画布信息
export function exportSmsStoreCanvas(query) {
    return request({
        url: '/sms/SmsStoreCanvas/export',
        method: 'get',
        params: query
    })
}
