import request from '@/utils/request'

// 查询视频列表列表
export function listSmsVideo(query) {
    return request({
        url: '/sms/SmsVideo/list',
        method: 'get',
        params: query
    })
}

// 查询视频列表详细
export function getSmsVideo(id) {
    return request({
        url: '/sms/SmsVideo/' + id,
        method: 'get'
    })
}

// 新增视频列表
export function addSmsVideo(data) {
    return request({
        url: '/sms/SmsVideo',
        method: 'post',
        data: data
    })
}

// 修改视频列表
export function updateSmsVideo(data) {
    return request({
        url: '/sms/SmsVideo',
        method: 'put',
        data: data
    })
}

// 删除视频列表
export function delSmsVideo(id) {
    return request({
        url: '/sms/SmsVideo/' + id,
        method: 'delete'
    })
}

// 导出视频列表
export function exportSmsVideo(query) {
    return request({
        url: '/sms/SmsVideo/export',
        method: 'get',
        params: query
    })
}