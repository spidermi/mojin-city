import request from '@/utils/request'

// 查询直播数据列表
export function listTLiveInfo(query) {
    return request({
        url: '/sms/TLiveInfo/list',
        method: 'get',
        params: query
    })
}

// 查询直播数据详细
export function getTLiveInfo(roomid) {
    return request({
        url: '/sms/TLiveInfo/' + roomid,
        method: 'get'
    })
}

// 新增直播数据
export function addTLiveInfo(data) {
    return request({
        url: '/sms/TLiveInfo',
        method: 'post',
        data: data
    })
}

// 修改直播数据
export function updateTLiveInfo(data) {
    return request({
        url: '/sms/TLiveInfo',
        method: 'put',
        data: data
    })
}

// 删除直播数据
export function delTLiveInfo(roomid) {
    return request({
        url: '/sms/TLiveInfo/' + roomid,
        method: 'delete'
    })
}

// 导出直播数据
export function exportTLiveInfo(query) {
    return request({
        url: '/sms/TLiveInfo/export',
        method: 'get',
        params: query
    })
}