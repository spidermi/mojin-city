// 导入api接口模块

// 获取当前环境变量 true => 生产环境 false => 开发环境
const BASEURL = (process.env.NODE_ENV === 'production') ? 'https://cloudapi.yixiang.co/' : '/dev-api/'

export const api = {
  // 画布模块
  fileUpload: BASEURL + 'system/upload/upload', // 文件上传
  getClassify: BASEURL + 'mall/canvas/getClassify', // 查询分类层级
  getProducts: BASEURL + 'mall/canvas/getProducts', // 选择商品查询
  saveCanvas: BASEURL + 'sms/SmsStoreCanvas', // 保存画布
  getCanvas: BASEURL + 'sms/SmsStoreCanvas/11', // 读取画布
  getShops: BASEURL + 'mall/canvas/getShops' // 选择店铺查询
}
export default api
