import request from '@/utils/request'

// 查询推广列表
export function listTcTuiguang(query) {
    return request({
        url: '/tc/TcTuiguang/list',
        method: 'get',
        params: query
    })
}

// 查询推广详细
export function getTcTuiguang(id) {
    return request({
        url: '/tc/TcTuiguang/' + id,
        method: 'get'
    })
}

// 新增推广
export function addTcTuiguang(data) {
    return request({
        url: '/tc/TcTuiguang',
        method: 'post',
        data: data
    })
}

// 修改推广
export function updateTcTuiguang(data) {
    return request({
        url: '/tc/TcTuiguang',
        method: 'put',
        data: data
    })
}

// 删除推广
export function delTcTuiguang(id) {
    return request({
        url: '/tc/TcTuiguang/' + id,
        method: 'delete'
    })
}

// 导出推广
export function exportTcTuiguang(query) {
    return request({
        url: '/tc/TcTuiguang/export',
        method: 'get',
        params: query
    })
}