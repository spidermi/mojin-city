import request from '@/utils/request'

// 查询农林牧渔列表
export function listTcNonglin(query) {
    return request({
        url: '/tc/TcNonglin/list',
        method: 'get',
        params: query
    })
}

// 查询农林牧渔详细
export function getTcNonglin(id) {
    return request({
        url: '/tc/TcNonglin/' + id,
        method: 'get'
    })
}

// 新增农林牧渔
export function addTcNonglin(data) {
    return request({
        url: '/tc/TcNonglin',
        method: 'post',
        data: data
    })
}

// 修改农林牧渔
export function updateTcNonglin(data) {
    return request({
        url: '/tc/TcNonglin',
        method: 'put',
        data: data
    })
}

// 删除农林牧渔
export function delTcNonglin(id) {
    return request({
        url: '/tc/TcNonglin/' + id,
        method: 'delete'
    })
}

// 导出农林牧渔
export function exportTcNonglin(query) {
    return request({
        url: '/tc/TcNonglin/export',
        method: 'get',
        params: query
    })
}