import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTcAd(query) {
    return request({
        url: '/tc/TcAd/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getTcAd(id) {
    return request({
        url: '/tc/TcAd/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addTcAd(data) {
    return request({
        url: '/tc/TcAd',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateTcAd(data) {
    return request({
        url: '/tc/TcAd',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delTcAd(id) {
    return request({
        url: '/tc/TcAd/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportTcAd(query) {
    return request({
        url: '/tc/TcAd/export',
        method: 'get',
        params: query
    })
}