import request from '@/utils/request'

// 查询订单列表
export function listTcOrder(query) {
    return request({
        url: '/tc/TcOrder/list',
        method: 'get',
        params: query
    })
}

// 查询订单详细
export function getTcOrder(id) {
    return request({
        url: '/tc/TcOrder/' + id,
        method: 'get'
    })
}

// 新增订单
export function addTcOrder(data) {
    return request({
        url: '/tc/TcOrder',
        method: 'post',
        data: data
    })
}

// 修改订单
export function updateTcOrder(data) {
    return request({
        url: '/tc/TcOrder',
        method: 'put',
        data: data
    })
}

// 删除订单
export function delTcOrder(id) {
    return request({
        url: '/tc/TcOrder/' + id,
        method: 'delete'
    })
}

// 导出订单
export function exportTcOrder(query) {
    return request({
        url: '/tc/TcOrder/export',
        method: 'get',
        params: query
    })
}