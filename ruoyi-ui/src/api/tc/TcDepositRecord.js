import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listTcDepositRecord(query) {
    return request({
        url: '/tc/TcDepositRecord/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getTcDepositRecord(id) {
    return request({
        url: '/tc/TcDepositRecord/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addTcDepositRecord(data) {
    return request({
        url: '/tc/TcDepositRecord',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateTcDepositRecord(data) {
    return request({
        url: '/tc/TcDepositRecord',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delTcDepositRecord(id) {
    return request({
        url: '/tc/TcDepositRecord/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportTcDepositRecord(query) {
    return request({
        url: '/tc/TcDepositRecord/export',
        method: 'get',
        params: query
    })
}