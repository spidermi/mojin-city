import request from '@/utils/request'

// 查询房产信息列表
export function listTcRoom(query) {
    return request({
        url: '/tc/TcRoom/list',
        method: 'get',
        params: query
    })
}

// 查询房产信息详细
export function getTcRoom(id) {
    return request({
        url: '/tc/TcRoom/' + id,
        method: 'get'
    })
}

// 新增房产信息
export function addTcRoom(data) {
    return request({
        url: '/tc/TcRoom',
        method: 'post',
        data: data
    })
}

// 修改房产信息
export function updateTcRoom(data) {
    return request({
        url: '/tc/TcRoom',
        method: 'put',
        data: data
    })
}

// 删除房产信息
export function delTcRoom(id) {
    return request({
        url: '/tc/TcRoom/' + id,
        method: 'delete'
    })
}

// 导出房产信息
export function exportTcRoom(query) {
    return request({
        url: '/tc/TcRoom/export',
        method: 'get',
        params: query
    })
}