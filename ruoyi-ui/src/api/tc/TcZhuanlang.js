import request from '@/utils/request'

// 查询转让列表
export function listTcZhuanlang(query) {
    return request({
        url: '/tc/TcZhuanlang/list',
        method: 'get',
        params: query
    })
}

// 查询转让详细
export function getTcZhuanlang(id) {
    return request({
        url: '/tc/TcZhuanlang/' + id,
        method: 'get'
    })
}

// 新增转让
export function addTcZhuanlang(data) {
    return request({
        url: '/tc/TcZhuanlang',
        method: 'post',
        data: data
    })
}

// 修改转让
export function updateTcZhuanlang(data) {
    return request({
        url: '/tc/TcZhuanlang',
        method: 'put',
        data: data
    })
}

// 删除转让
export function delTcZhuanlang(id) {
    return request({
        url: '/tc/TcZhuanlang/' + id,
        method: 'delete'
    })
}

// 导出转让
export function exportTcZhuanlang(query) {
    return request({
        url: '/tc/TcZhuanlang/export',
        method: 'get',
        params: query
    })
}