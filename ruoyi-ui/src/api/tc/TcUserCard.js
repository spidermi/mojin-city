import request from '@/utils/request'

// 查询人才简历列表
export function listTcUserCard(query) {
    return request({
        url: '/tc/TcUserCard/list',
        method: 'get',
        params: query
    })
}

// 查询人才简历详细
export function getTcUserCard(id) {
    return request({
        url: '/tc/TcUserCard/' + id,
        method: 'get'
    })
}

// 新增人才简历
export function addTcUserCard(data) {
    return request({
        url: '/tc/TcUserCard',
        method: 'post',
        data: data
    })
}

// 修改人才简历
export function updateTcUserCard(data) {
    return request({
        url: '/tc/TcUserCard',
        method: 'put',
        data: data
    })
}

// 删除人才简历
export function delTcUserCard(id) {
    return request({
        url: '/tc/TcUserCard/' + id,
        method: 'delete'
    })
}

// 导出人才简历
export function exportTcUserCard(query) {
    return request({
        url: '/tc/TcUserCard/export',
        method: 'get',
        params: query
    })
}