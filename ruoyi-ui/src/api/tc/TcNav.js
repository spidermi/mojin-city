import request from '@/utils/request'

// 查询模块列表
export function listTcNav(query) {
    return request({
        url: '/tc/TcNav/list',
        method: 'get',
        params: query
    })
}

// 查询模块详细
export function getTcNav(id) {
    return request({
        url: '/tc/TcNav/' + id,
        method: 'get'
    })
}

// 新增模块
export function addTcNav(data) {
    return request({
        url: '/tc/TcNav',
        method: 'post',
        data: data
    })
}

// 修改模块
export function updateTcNav(data) {
    return request({
        url: '/tc/TcNav',
        method: 'put',
        data: data
    })
}

// 删除模块
export function delTcNav(id) {
    return request({
        url: '/tc/TcNav/' + id,
        method: 'delete'
    })
}

// 导出模块
export function exportTcNav(query) {
    return request({
        url: '/tc/TcNav/export',
        method: 'get',
        params: query
    })
}