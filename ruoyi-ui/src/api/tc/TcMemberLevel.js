import request from '@/utils/request'

// 查询vip等级列表
export function listTcMemberLevel(query) {
    return request({
        url: '/tc/TcMemberLevel/list',
        method: 'get',
        params: query
    })
}

// 查询vip等级详细
export function getTcMemberLevel(id) {
    return request({
        url: '/tc/TcMemberLevel/' + id,
        method: 'get'
    })
}

// 新增vip等级
export function addTcMemberLevel(data) {
    return request({
        url: '/tc/TcMemberLevel',
        method: 'post',
        data: data
    })
}

// 修改vip等级
export function updateTcMemberLevel(data) {
    return request({
        url: '/tc/TcMemberLevel',
        method: 'put',
        data: data
    })
}

// 删除vip等级
export function delTcMemberLevel(id) {
    return request({
        url: '/tc/TcMemberLevel/' + id,
        method: 'delete'
    })
}

// 导出vip等级
export function exportTcMemberLevel(query) {
    return request({
        url: '/tc/TcMemberLevel/export',
        method: 'get',
        params: query
    })
}