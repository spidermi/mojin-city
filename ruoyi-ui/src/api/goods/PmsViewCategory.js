import request from '@/utils/request'

// 查询二级分类浏览记录列表
export function listPmsViewCategory(query) {
    return request({
        url: '/pms/PmsViewCategory/list',
        method: 'get',
        params: query
    })
}

// 查询二级分类浏览记录详细
export function getPmsViewCategory(userId) {
    return request({
        url: '/pms/PmsViewCategory/' + userId,
        method: 'get'
    })
}

// 新增二级分类浏览记录
export function addPmsViewCategory(data) {
    return request({
        url: '/pms/PmsViewCategory',
        method: 'post',
        data: data
    })
}

// 修改二级分类浏览记录
export function updatePmsViewCategory(data) {
    return request({
        url: '/pms/PmsViewCategory',
        method: 'put',
        data: data
    })
}

// 删除二级分类浏览记录
export function delPmsViewCategory(userId) {
    return request({
        url: '/pms/PmsViewCategory/' + userId,
        method: 'delete'
    })
}

// 导出二级分类浏览记录
export function exportPmsViewCategory(query) {
    return request({
        url: '/pms/PmsViewCategory/export',
        method: 'get',
        params: query
    })
}