import request from '@/utils/request'

// 查询视频课程列表
export function listElVideo(query) {
    return request({
        url: '/exam/ElVideo/list',
        method: 'get',
        params: query
    })
}

// 查询视频课程详细
export function getElVideo(id) {
    return request({
        url: '/exam/ElVideo/' + id,
        method: 'get'
    })
}

// 新增视频课程
export function addElVideo(data) {
    return request({
        url: '/exam/ElVideo',
        method: 'post',
        data: data
    })
}

// 修改视频课程
export function updateElVideo(data) {
    return request({
        url: '/exam/ElVideo',
        method: 'put',
        data: data
    })
}

// 删除视频课程
export function delElVideo(id) {
    return request({
        url: '/exam/ElVideo/' + id,
        method: 'delete'
    })
}

// 导出视频课程
export function exportElVideo(query) {
    return request({
        url: '/exam/ElVideo/export',
        method: 'get',
        params: query
    })
}
