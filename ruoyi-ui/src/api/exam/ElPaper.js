import {get,post} from '@/utils/request'
import request from '@/utils/request'
// 查询试卷列表
export function listElPaper (query) {
  return request({
    url: '/exam/ElPaper/list',
    method: 'get',
    params: query
  })
}

// 查询试卷详细
export function getElPaper (id) {
  return request({
    url: '/exam/ElPaper/' + id,
    method: 'get'
  })
}

// 新增试卷
export function addElPaper (data) {
  return request({
    url: '/exam/ElPaper',
    method: 'post',
    data: data
  })
}

// 修改试卷
export function updateElPaper (data) {
  return request({
    url: '/exam/ElPaper',
    method: 'put',
    data: data
  })
}

// 删除试卷
export function delElPaper (id) {
  return request({
    url: '/exam/ElPaper/' + id,
    method: 'delete'
  })
}

// 导出试卷
export function exportElPaper (query) {
  return request({
    url: '/exam/ElPaper/export',
    method: 'get',
    params: query
  })
}
/**
 * 创建试卷
 * @param data
 */
export function createPaper(data) {
  return post('/exam/ElPaper/create-paper', data)
}

/**
 * 试卷详情
 * @param data
 */
export function paperDetail(data) {
  return post('/exam/ElPaper/paper-detail', data)
}

/**
 * 题目详情
 * @param data
 */
export function quDetail(data) {
  return post('/exam/ElPaper/qu-detail', data)
}

/**
 * 填充答案
 * @param data
 */
export function fillAnswer(data) {
  return post('/exam/ElPaper/fill-answer', data)
}

/**
 * 交卷
 * @param data
 */
export function handExam(data) {
  return post('/exam/ElPaper/hand-exam', data)
}

/**
 * 试卷详情
 * @param data
 */
export function paperResult(data) {
  return post('/exam/ElPaper/paper-result', data)
}

/**
 * 错题训练
 * @param data
 */
export function training(data) {
  return post('/exam/ElPaper/training', data)
}
