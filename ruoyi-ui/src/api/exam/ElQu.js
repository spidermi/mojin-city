import request from '@/utils/request'

// 查询问题题目列表
export function listElQu (query) {
  return request({
    url: '/exam/ElQu/list',
    method: 'get',
    params: query
  })
}

// 查询问题题目详细
export function getElQu (id) {
  return request({
    url: '/exam/ElQu/' + id,
    method: 'get'
  })
}

// 新增问题题目
export function addElQu (data) {
  return request({
    url: '/exam/ElQu',
    method: 'post',
    data: data
  })
}

// 修改问题题目
export function updateElQu (data) {
  return request({
    url: '/exam/ElQu',
    method: 'put',
    data: data
  })
}

// 删除问题题目
export function delElQu (id) {
  return request({
    url: '/exam/ElQu/' + id,
    method: 'delete'
  })
}

// 导出问题题目
export function exportElQu (query) {
  return request({
    url: '/exam/ElQu/export',
    method: 'get',
    params: query
  })
}
