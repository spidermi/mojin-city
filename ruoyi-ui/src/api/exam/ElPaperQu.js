import request from '@/utils/request'

// 查询试卷考题列表
export function listElPaperQu (query) {
  return request({
    url: '/exam/ElPaperQu/list',
    method: 'get',
    params: query
  })
}

// 查询试卷考题详细
export function getElPaperQu (id) {
  return request({
    url: '/exam/ElPaperQu/' + id,
    method: 'get'
  })
}

// 新增试卷考题
export function addElPaperQu (data) {
  return request({
    url: '/exam/ElPaperQu',
    method: 'post',
    data: data
  })
}

// 修改试卷考题
export function updateElPaperQu (data) {
  return request({
    url: '/exam/ElPaperQu',
    method: 'put',
    data: data
  })
}

// 删除试卷考题
export function delElPaperQu (id) {
  return request({
    url: '/exam/ElPaperQu/' + id,
    method: 'delete'
  })
}

// 导出试卷考题
export function exportElPaperQu (query) {
  return request({
    url: '/exam/ElPaperQu/export',
    method: 'get',
    params: query
  })
}
