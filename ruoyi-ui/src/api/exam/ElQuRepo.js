import request from '@/utils/request'

// 查询试题题库列表
export function listElQuRepo (query) {
  return request({
    url: '/exam/ElQuRepo/list',
    method: 'get',
    params: query
  })
}

// 查询试题题库详细
export function getElQuRepo (id) {
  return request({
    url: '/exam/ElQuRepo/' + id,
    method: 'get'
  })
}

// 新增试题题库
export function addElQuRepo (data) {
  return request({
    url: '/exam/ElQuRepo',
    method: 'post',
    data: data
  })
}

// 修改试题题库
export function updateElQuRepo (data) {
  return request({
    url: '/exam/ElQuRepo',
    method: 'put',
    data: data
  })
}

// 删除试题题库
export function delElQuRepo (id) {
  return request({
    url: '/exam/ElQuRepo/' + id,
    method: 'delete'
  })
}

// 导出试题题库
export function exportElQuRepo (query) {
  return request({
    url: '/exam/ElQuRepo/export',
    method: 'get',
    params: query
  })
}
