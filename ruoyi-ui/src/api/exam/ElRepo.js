import request from '@/utils/request'

// 查询题库列表
export function listElRepo (query) {
  return request({
    url: '/exam/ElRepo/list',
    method: 'get',
    params: query
  })
}

// 查询题库详细
export function getElRepo (id) {
  return request({
    url: '/exam/ElRepo/' + id,
    method: 'get'
  })
}

// 新增题库
export function addElRepo (data) {
  return request({
    url: '/exam/ElRepo',
    method: 'post',
    data: data
  })
}

// 修改题库
export function updateElRepo (data) {
  return request({
    url: '/exam/ElRepo',
    method: 'put',
    data: data
  })
}

// 删除题库
export function delElRepo (id) {
  return request({
    url: '/exam/ElRepo/' + id,
    method: 'delete'
  })
}

// 导出题库
export function exportElRepo (query) {
  return request({
    url: '/exam/ElRepo/export',
    method: 'get',
    params: query
  })
}
