import request from '@/utils/request'

// 查询账单记录列表
export function listOmsBillingRecords(query) {
  return request({
    url: '/order/OmsBillingRecords/list',
    method: 'get',
    params: query
  })
}

// 查询账单记录详细
export function getOmsBillingRecords(id) {
  return request({
    url: '/order/OmsBillingRecords/' + id,
    method: 'get'
  })
}

// 新增账单记录
export function addOmsBillingRecords(data) {
  return request({
    url: '/order/OmsBillingRecords',
    method: 'post',
    data: data
  })
}

// 修改账单记录
export function updateOmsBillingRecords(data) {
  return request({
    url: '/order/OmsBillingRecords',
    method: 'put',
    data: data
  })
}

// 删除账单记录
export function delOmsBillingRecords(id) {
  return request({
    url: '/order/OmsBillingRecords/' + id,
    method: 'delete'
  })
}

// 导出账单记录
export function exportOmsBillingRecords(query) {
  return request({
    url: '/order/OmsBillingRecords/export',
    method: 'get',
    params: query
  })
}

// 分页查询店铺账单
export function storebillings(query) {
  return request({
    url: '/order/OmsBillingRecords/storebillings',
    method: 'get',
    params: query
  })
}// 分页查询店铺对账记录
export function storebillingrecords(query) {
  return request({
    url: '/order/OmsBillingRecords/storebillingrecords',
    method: 'get',
    params: query
  })
}
/**
 * 设置店铺对账记录状态为已结算
 *
 * @param ids 对账记录id数组
 * @return 成功>=1 否则失败
 */
export function updateStoreBillingRecordStatus(ids) {
  return request({
    url: '/order/OmsBillingRecords/storebillingrecord',
    method: 'put',
    params: ids,
  })
}
