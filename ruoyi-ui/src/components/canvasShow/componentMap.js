// export const componentMap = [
//   // 小程序
//   new Map([
//     ['header', () => import('@/components/canvasShow/yshop/Header')], // 首页头部
//     ['search', () => import('@/components/canvasShow/yshop/Search')], // 搜索
//     ['text', () => import('@/components/canvasShow/basics/text')], // 文本
//     ['imageText', () => import('@/components/canvasShow/basics/imageText')], // 图文
//     ['brandList', () => import('@/components/canvasShow/basics/brandList')], // 品牌列表
//     ['categoryList', () => import('@/components/canvasShow/basics/categoryList')], // 品牌列表
//     ['imageTextList', () => import('@/components/canvasShow/basics/imageTextList')], // 图文列表
//     ['productList', () => import('@/components/canvasShow/basics/productList')], // 商品列表
//     ['videoBox', () => import('@/components/canvasShow/basics/video')], // 视频
//     ['coupon', () => import('@/components/canvasShow/basics/coupon')], // 优惠券
//     ['custom', () => import('@/components/canvasShow/basics/custom')] // 优惠券
//   ]),
//   // H5
//   new Map([
//     ['header', () => import('@/components/canvasShow/yshop/Header')], // 首页头部
//     ['search', () => import('@/components/canvasShow/yshop/Search')], // 搜索
//     ['text', () => import('@/components/canvasShow/basics/text')], // 文本
//     ['imageText', () => import('@/components/canvasShow/basics/imageText')], // 图文
//     ['brandList', () => import('@/components/canvasShow/basics/brandList')], // 品牌列表
//     ['categoryList', () => import('@/components/canvasShow/basics/categoryList')], // 品牌列表
//     ['imageTextList', () => import('@/components/canvasShow/basics/imageTextList')], // 图文列表
//     ['productList', () => import('@/components/canvasShow/basics/productList')], // 商品列表
//     ['videoBox', () => import('@/components/canvasShow/basics/video')], // 视频
//     ['coupon', () => import('@/components/canvasShow/basics/coupon')], // 优惠券
//     ['custom', () => import('@/components/canvasShow/basics/custom')] // 优惠券
//   ]),
//   // APP
//   new Map([
//     ['header', () => import('@/components/canvasShow/yshop/Header')], // 首页头部
//     ['search', () => import('@/components/canvasShow/yshop/Search')], // 搜索
//     ['text', () => import('@/components/canvasShow/basics/text')], // 文本
//     ['imageText', () => import('@/components/canvasShow/basics/imageText')], // 图文
//     ['brandList', () => import('@/components/canvasShow/basics/brandList')], // 品牌列表
//     ['categoryList', () => import('@/components/canvasShow/basics/categoryList')], // 品牌列表
//     ['imageTextList', () => import('@/components/canvasShow/basics/imageTextList')], // 图文列表
//     ['productList', () => import('@/components/canvasShow/basics/productList')], // 商品列表
//     ['videoBox', () => import('@/components/canvasShow/basics/video')], // 视频
//     ['coupon', () => import('@/components/canvasShow/basics/coupon')], // 优惠券
//     ['custom', () => import('@/components/canvasShow/basics/custom')] // 优惠券
//   ]),
//   // PC
//   new Map([
//     ['header', () => import('@/components/canvasShow/yshop/Header')], // 首页头部
//     ['search', () => import('@/components/canvasShow/yshop/Search')], // 搜索
//     ['text', () => import('@/components/canvasShow/basics/text')], // 文本
//     ['imageText', () => import('@/components/canvasShow/basics/imageText')], // 图文
//     ['brandList', () => import('@/components/canvasShow/basics/brandList')], // 品牌列表
//     ['categoryList', () => import('@/components/canvasShow/basics/categoryList')], // 品牌列表
//     ['imageTextList', () => import('@/components/canvasShow/basics/imageTextList')], // 图文列表
//     ['productList', () => import('@/components/canvasShow/basics/productList')], // 商品列表
//     ['videoBox', () => import('@/components/canvasShow/basics/video')], // 视频
//     ['coupon', () => import('@/components/canvasShow/basics/coupon')], // 优惠券
//     ['custom', () => import('@/components/canvasShow/basics/custom')] // 优惠券
//   ])
// ]
// export default componentMap

// 小程序
const componentMap = new Map([
  ['header', () => import('@/components/canvasShow/yshop/Header.vue')], // 店铺头部
  ['search', () => import('@/components/canvasShow/yshop/Search.vue')], // 搜索商品
  ['banner', () => import('@/components/canvasShow/yshop/Banner.vue')], // 轮播图
  ['noticeBar', () => import('@/components/canvasShow/yshop/NoticeBar.vue')], // 滚动新闻
  ['menu', () => import('@/components/canvasShow/yshop/Menu.vue')], // 菜单
  ['adv', () => import('@/components/canvasShow/yshop/Adv.vue')], // 广告
  ['groupon', () => import('@/components/canvasShow/yshop/Groupon.vue')], // 超值拼团
  [
    'hotCommodity',
    () => import('@/components/canvasShow/yshop/HotCommodity.vue')
  ], // 热门榜单
  [
    'firstNewProduct',
    () => import('@/components/canvasShow/yshop/FirstNewProduct.vue')
  ], // 秒杀
  [
    'productsRecommended',
    () => import('@/components/canvasShow/yshop/ProductsRecommended.vue')
  ], // 精品推荐
  [
    'promoteProduct',
    () => import('@/components/canvasShow/yshop/PromoteProduct.vue')
  ], // 促销单品
  ['live', () => import('@/components/canvasShow/yshop/Live.vue')], // 直播
  [
    'promotionGood',
    () => import('@/components/canvasShow/yshop/PromotionGood.vue')
  ] // 为您推荐
])

export default componentMap
