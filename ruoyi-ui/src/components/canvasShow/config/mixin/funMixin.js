/*
 * 公共方法的 mixin
 */

/* eslint-disable */
export const tool = {
  methods: {
    // 判断url
    getLink (linkObj) {
      var link = ''
      if(linkObj.typeText && linkObj.data){
        switch (linkObj.typeText) {
          case '类别':
            link = {name:'category',params:{classifyData:linkObj.data}}
            break
          case '店辅':
            link = {name:'store',params:{shopData:linkObj.data}}
            break
          case '商品':
            link = {name:'productDetail',params:{productData:linkObj.data}}
            break
        }
      }
      return link
    }
  }
}
