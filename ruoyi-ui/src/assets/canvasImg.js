//  左侧工具栏图标
import shopIcon from './canvasImg/panel/dainpu@2x.png'
import textIcon from './canvasImg/panel/wenben@2x.png'
import brandIcon from './canvasImg/panel/pinpai@2x.png'
import textImgIcon from './canvasImg/panel/tuwen@2x.png'
import twListIcon from './canvasImg/panel/twlieb@2x.png'
import customIcon from './canvasImg/panel/didingy@2x.png'
import videoIcon from './canvasImg/panel/ship@2x.png'
import goodsList from './canvasImg/panel/shangpinlieb@2x.png'
import categoryIcon from './canvasImg/panel/leibie@2x.png'
import coupon from './canvasImg/panel/youhuiq@2x.png'
// 左侧工具选中状态
import shopIconH from './canvasImg/panel/dainpu1@2x.png'
import textIconH from './canvasImg/panel/wenben1@2x.png'
import brandIconH from './canvasImg/panel/pinpai1@2x.png'
import textImgIconH from './canvasImg/panel/tuwen1@2x.png'
import twListIconH from './canvasImg/panel/twlieb1@2x.png'
import customIconH from './canvasImg/panel/didingy1@2x.png'
import videoIconH from './canvasImg/panel/ship1@2x.png'
import goodsListH from './canvasImg/panel/shangpinlieb1@2x.png'
import categoryIconH from './canvasImg/panel/leibie1@2x.png'
import couponH from './canvasImg/panel/youhuiq1@2x.png'

// 模板测试图片
import pinpaiImg from './images/cereshop/home/pinpaiImg.png'

export default{
  shopIcon,
  textIcon,
  brandIcon,
  textImgIcon,
  twListIcon,
  customIcon,
  videoIcon,
  goodsList,
  categoryIcon,
  coupon,
  shopIconH,
  textIconH,
  brandIconH,
  textImgIconH,
  twListIconH,
  customIconH,
  videoIconH,
  goodsListH,
  categoryIconH,
  couponH,
  pinpaiImg
}
