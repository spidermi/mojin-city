package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcCar;
import com.ruoyi.tc.service.ITcCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 房产信息Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcCar")
public class TcCarController extends BaseController {
    @Autowired
    private ITcCarService tcCarService;

    /**
     * 查询房产信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCar:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcCar tcCar) {
        startPage();
        List<TcCar> list = tcCarService.selectTcCarList(tcCar);
        return getDataTable(list);
    }

    /**
     * 导出房产信息列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCar:export')")
    @Log(title = "房产信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcCar tcCar) {
        List<TcCar> list = tcCarService.selectTcCarList(tcCar);
        ExcelUtil<TcCar> util = new ExcelUtil<TcCar>(TcCar.class);
        return util.exportExcel(list, "TcCar");
    }

    /**
     * 获取房产信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCar:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcCarService.selectTcCarById(id));
    }

    /**
     * 新增房产信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCar:add')")
    @Log(title = "房产信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcCar tcCar) {
        return toAjax(tcCarService.insertTcCar(tcCar));
    }

    /**
     * 修改房产信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCar:edit')")
    @Log(title = "房产信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcCar tcCar) {
        return toAjax(tcCarService.updateTcCar(tcCar));
    }

    /**
     * 删除房产信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcCar:remove')")
    @Log(title = "房产信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcCarService.deleteTcCarByIds(ids));
    }
}
