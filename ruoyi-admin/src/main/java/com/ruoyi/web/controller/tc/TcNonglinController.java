package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcNonglin;
import com.ruoyi.tc.service.ITcNonglinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 农林牧渔Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcNonglin")
public class TcNonglinController extends BaseController {
    @Autowired
    private ITcNonglinService tcNonglinService;

    /**
     * 查询农林牧渔列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNonglin:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcNonglin tcNonglin) {
        startPage();
        List<TcNonglin> list = tcNonglinService.selectTcNonglinList(tcNonglin);
        return getDataTable(list);
    }

    /**
     * 导出农林牧渔列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNonglin:export')")
    @Log(title = "农林牧渔", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcNonglin tcNonglin) {
        List<TcNonglin> list = tcNonglinService.selectTcNonglinList(tcNonglin);
        ExcelUtil<TcNonglin> util = new ExcelUtil<TcNonglin>(TcNonglin.class);
        return util.exportExcel(list, "TcNonglin");
    }

    /**
     * 获取农林牧渔详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNonglin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcNonglinService.selectTcNonglinById(id));
    }

    /**
     * 新增农林牧渔
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNonglin:add')")
    @Log(title = "农林牧渔", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcNonglin tcNonglin) {
        return toAjax(tcNonglinService.insertTcNonglin(tcNonglin));
    }

    /**
     * 修改农林牧渔
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNonglin:edit')")
    @Log(title = "农林牧渔", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcNonglin tcNonglin) {
        return toAjax(tcNonglinService.updateTcNonglin(tcNonglin));
    }

    /**
     * 删除农林牧渔
     */
    @PreAuthorize("@ss.hasPermi('tc:TcNonglin:remove')")
    @Log(title = "农林牧渔", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcNonglinService.deleteTcNonglinByIds(ids));
    }
}
