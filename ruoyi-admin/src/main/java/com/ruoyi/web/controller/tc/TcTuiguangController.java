package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcGoods;
import com.ruoyi.tc.domain.TcTuiguang;
import com.ruoyi.tc.service.ITcTuiguangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 推广Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcTuiguang")
public class TcTuiguangController extends BaseController {
    @Autowired
    private ITcTuiguangService tcTuiguangService;

    /**
     * 查询推广列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcTuiguang:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcTuiguang tcTuiguang) {
        startPage();
        List<TcTuiguang> list = tcTuiguangService.selectTcTuiguangList(tcTuiguang);
        return getDataTable(list);
    }

    /**
     * 导出推广列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcTuiguang:export')")
    @Log(title = "推广", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcTuiguang tcTuiguang) {
        List<TcTuiguang> list = tcTuiguangService.selectTcTuiguangList(tcTuiguang);
        ExcelUtil<TcTuiguang> util = new ExcelUtil<TcTuiguang>(TcTuiguang.class);
        return util.exportExcel(list, "TcTuiguang");
    }

    /**
     * 获取推广详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcTuiguang:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        TcTuiguang tcGoods =tcTuiguangService.selectTcTuiguangById(id);
        if (StringUtils.isNotEmpty(tcGoods.getTagIds())){
            tcGoods.setTagIdsIds(tcGoods.getTagIds().split(","));
        }
        return AjaxResult.success(tcGoods);

    }

    /**
     * 新增推广
     */
    @PreAuthorize("@ss.hasPermi('tc:TcTuiguang:add')")
    @Log(title = "推广", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcTuiguang tcTuiguang) {

        return toAjax(tcTuiguangService.insertTcTuiguang(tcTuiguang));
    }

    /**
     * 修改推广
     */
    @PreAuthorize("@ss.hasPermi('tc:TcTuiguang:edit')")
    @Log(title = "推广", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcTuiguang tcTuiguang) {

        return toAjax(tcTuiguangService.updateTcTuiguang(tcTuiguang));
    }

    /**
     * 删除推广
     */
    @PreAuthorize("@ss.hasPermi('tc:TcTuiguang:remove')")
    @Log(title = "推广", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcTuiguangService.deleteTcTuiguangByIds(ids));
    }
}
