package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcUserCard;
import com.ruoyi.tc.service.ITcUserCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 人才简历Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcUserCard")
public class TcUserCardController extends BaseController {
    @Autowired
    private ITcUserCardService tcUserCardService;

    /**
     * 查询人才简历列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcUserCard:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcUserCard tcUserCard) {
        startPage();
        List<TcUserCard> list = tcUserCardService.selectTcUserCardList(tcUserCard);
        return getDataTable(list);
    }

    /**
     * 导出人才简历列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcUserCard:export')")
    @Log(title = "人才简历", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcUserCard tcUserCard) {
        List<TcUserCard> list = tcUserCardService.selectTcUserCardList(tcUserCard);
        ExcelUtil<TcUserCard> util = new ExcelUtil<TcUserCard>(TcUserCard.class);
        return util.exportExcel(list, "TcUserCard");
    }

    /**
     * 获取人才简历详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcUserCard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcUserCardService.selectTcUserCardById(id));
    }

    /**
     * 新增人才简历
     */
    @PreAuthorize("@ss.hasPermi('tc:TcUserCard:add')")
    @Log(title = "人才简历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcUserCard tcUserCard) {
        return toAjax(tcUserCardService.insertTcUserCard(tcUserCard));
    }

    /**
     * 修改人才简历
     */
    @PreAuthorize("@ss.hasPermi('tc:TcUserCard:edit')")
    @Log(title = "人才简历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcUserCard tcUserCard) {
        return toAjax(tcUserCardService.updateTcUserCard(tcUserCard));
    }

    /**
     * 删除人才简历
     */
    @PreAuthorize("@ss.hasPermi('tc:TcUserCard:remove')")
    @Log(title = "人才简历", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcUserCardService.deleteTcUserCardByIds(ids));
    }
}
