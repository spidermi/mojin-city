package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcMemberLevel;
import com.ruoyi.tc.service.ITcMemberLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * vip等级Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcMemberLevel")
public class TcMemberLevelController extends BaseController {
    @Autowired
    private ITcMemberLevelService tcMemberLevelService;

    /**
     * 查询vip等级列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcMemberLevel:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcMemberLevel tcMemberLevel) {
        startPage();
        List<TcMemberLevel> list = tcMemberLevelService.selectTcMemberLevelList(tcMemberLevel);
        return getDataTable(list);
    }

    /**
     * 导出vip等级列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcMemberLevel:export')")
    @Log(title = "vip等级", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcMemberLevel tcMemberLevel) {
        List<TcMemberLevel> list = tcMemberLevelService.selectTcMemberLevelList(tcMemberLevel);
        ExcelUtil<TcMemberLevel> util = new ExcelUtil<TcMemberLevel>(TcMemberLevel.class);
        return util.exportExcel(list, "TcMemberLevel");
    }

    /**
     * 获取vip等级详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcMemberLevel:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcMemberLevelService.selectTcMemberLevelById(id));
    }

    /**
     * 新增vip等级
     */
    @PreAuthorize("@ss.hasPermi('tc:TcMemberLevel:add')")
    @Log(title = "vip等级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcMemberLevel tcMemberLevel) {
        return toAjax(tcMemberLevelService.insertTcMemberLevel(tcMemberLevel));
    }

    /**
     * 修改vip等级
     */
    @PreAuthorize("@ss.hasPermi('tc:TcMemberLevel:edit')")
    @Log(title = "vip等级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcMemberLevel tcMemberLevel) {
        return toAjax(tcMemberLevelService.updateTcMemberLevel(tcMemberLevel));
    }

    /**
     * 删除vip等级
     */
    @PreAuthorize("@ss.hasPermi('tc:TcMemberLevel:remove')")
    @Log(title = "vip等级", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcMemberLevelService.deleteTcMemberLevelByIds(ids));
    }
}
