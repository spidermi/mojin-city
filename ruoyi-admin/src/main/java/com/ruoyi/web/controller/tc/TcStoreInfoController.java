package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcStoreInfo;
import com.ruoyi.tc.service.ITcStoreInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商家店铺Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcStoreInfo")
public class TcStoreInfoController extends BaseController {
    @Autowired
    private ITcStoreInfoService tcStoreInfoService;

    /**
     * 查询商家店铺列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStoreInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcStoreInfo tcStoreInfo) {
        startPage();
        List<TcStoreInfo> list = tcStoreInfoService.selectTcStoreInfoList(tcStoreInfo);
        return getDataTable(list);
    }

    /**
     * 导出商家店铺列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStoreInfo:export')")
    @Log(title = "商家店铺", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcStoreInfo tcStoreInfo) {
        List<TcStoreInfo> list = tcStoreInfoService.selectTcStoreInfoList(tcStoreInfo);
        ExcelUtil<TcStoreInfo> util = new ExcelUtil<TcStoreInfo>(TcStoreInfo.class);
        return util.exportExcel(list, "TcStoreInfo");
    }

    /**
     * 获取商家店铺详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStoreInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcStoreInfoService.selectTcStoreInfoById(id));
    }

    /**
     * 新增商家店铺
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStoreInfo:add')")
    @Log(title = "商家店铺", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcStoreInfo tcStoreInfo) {
        return toAjax(tcStoreInfoService.insertTcStoreInfo(tcStoreInfo));
    }

    /**
     * 修改商家店铺
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStoreInfo:edit')")
    @Log(title = "商家店铺", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcStoreInfo tcStoreInfo) {
        return toAjax(tcStoreInfoService.updateTcStoreInfo(tcStoreInfo));
    }

    /**
     * 删除商家店铺
     */
    @PreAuthorize("@ss.hasPermi('tc:TcStoreInfo:remove')")
    @Log(title = "商家店铺", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcStoreInfoService.deleteTcStoreInfoByIds(ids));
    }
}
