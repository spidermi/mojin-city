package com.ruoyi.web.controller.tc;

import java.util.List;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.tc.domain.TcOrder;
import com.ruoyi.tc.service.ITcOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单Controller
 *
 * @author é­éåå
 * @date 2022-01-23
 */
@RestController
@RequestMapping("/tc/TcOrder")
public class TcOrderController extends BaseController {
    @Autowired
    private ITcOrderService tcOrderService;

/**
 * 查询订单列表
 */
@PreAuthorize("@ss.hasPermi('tc:TcOrder:list')")
@GetMapping("/list")
        public TableDataInfo list(TcOrder tcOrder) {
        startPage();
        if (StringUtils.isNotEmpty(tcOrder.getType())){
            tcOrder.setType(tcOrder.getType().split(",")[0]);
        }
        List<TcOrder> list = tcOrderService.selectTcOrderList(tcOrder);
        return getDataTable(list);
    }
    
    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcOrder:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcOrder tcOrder) {
        List<TcOrder> list = tcOrderService.selectTcOrderList(tcOrder);
        ExcelUtil<TcOrder> util = new ExcelUtil<TcOrder>(TcOrder. class);
        return util.exportExcel(list, "TcOrder");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcOrderService.selectTcOrderById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('tc:TcOrder:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcOrder tcOrder) {
        return toAjax(tcOrderService.insertTcOrder(tcOrder));
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('tc:TcOrder:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcOrder tcOrder) {
        return toAjax(tcOrderService.updateTcOrder(tcOrder));
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('tc:TcOrder:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcOrderService.deleteTcOrderByIds(ids));
    }
}
