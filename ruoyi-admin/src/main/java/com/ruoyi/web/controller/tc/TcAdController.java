package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcAd;
import com.ruoyi.tc.service.ITcAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcAd")
public class TcAdController extends BaseController {
    @Autowired
    private ITcAdService tcAdService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcAd:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcAd tcAd) {
        startPage();
        List<TcAd> list = tcAdService.selectTcAdList(tcAd);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcAd:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcAd tcAd) {
        List<TcAd> list = tcAdService.selectTcAdList(tcAd);
        ExcelUtil<TcAd> util = new ExcelUtil<TcAd>(TcAd.class);
        return util.exportExcel(list, "TcAd");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcAd:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcAdService.selectTcAdById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcAd:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcAd tcAd) {
        return toAjax(tcAdService.insertTcAd(tcAd));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcAd:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcAd tcAd) {
        return toAjax(tcAdService.updateTcAd(tcAd));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('tc:TcAd:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcAdService.deleteTcAdByIds(ids));
    }
}
