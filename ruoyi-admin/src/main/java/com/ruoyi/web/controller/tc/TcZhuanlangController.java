package com.ruoyi.web.controller.tc;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ConvertUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.tc.domain.TcZhuanlang;
import com.ruoyi.tc.service.ITcZhuanlangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 转让Controller
 *
 * @author é­éåå
 * @date 2022-01-21
 */
@RestController
@RequestMapping("/tc/TcZhuanlang")
public class TcZhuanlangController extends BaseController {
    @Autowired
    private ITcZhuanlangService tcZhuanlangService;

    /**
     * 查询转让列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcZhuanlang:list')")
    @GetMapping("/list")
    public TableDataInfo list(TcZhuanlang tcZhuanlang) {
        startPage();
        List<TcZhuanlang> list = tcZhuanlangService.selectTcZhuanlangList(tcZhuanlang);
        return getDataTable(list);
    }

    /**
     * 导出转让列表
     */
    @PreAuthorize("@ss.hasPermi('tc:TcZhuanlang:export')")
    @Log(title = "转让", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TcZhuanlang tcZhuanlang) {
        List<TcZhuanlang> list = tcZhuanlangService.selectTcZhuanlangList(tcZhuanlang);
        ExcelUtil<TcZhuanlang> util = new ExcelUtil<TcZhuanlang>(TcZhuanlang.class);
        return util.exportExcel(list, "TcZhuanlang");
    }

    /**
     * 获取转让详细信息
     */
    @PreAuthorize("@ss.hasPermi('tc:TcZhuanlang:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tcZhuanlangService.selectTcZhuanlangById(id));
    }

    /**
     * 新增转让
     */
    @PreAuthorize("@ss.hasPermi('tc:TcZhuanlang:add')")
    @Log(title = "转让", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TcZhuanlang tcZhuanlang) {
        return toAjax(tcZhuanlangService.insertTcZhuanlang(tcZhuanlang));
    }

    /**
     * 修改转让
     */
    @PreAuthorize("@ss.hasPermi('tc:TcZhuanlang:edit')")
    @Log(title = "转让", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TcZhuanlang tcZhuanlang) {
        return toAjax(tcZhuanlangService.updateTcZhuanlang(tcZhuanlang));
    }

    /**
     * 删除转让
     */
    @PreAuthorize("@ss.hasPermi('tc:TcZhuanlang:remove')")
    @Log(title = "转让", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tcZhuanlangService.deleteTcZhuanlangByIds(ids));
    }
}
