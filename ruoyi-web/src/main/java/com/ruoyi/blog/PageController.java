package com.ruoyi.blog;

import com.ruoyi.cms.bean.ColumnList;
import com.ruoyi.cms.service.ArticleListService;
import com.ruoyi.cms.service.ColumnListService;
import com.ruoyi.common.annotation.UnAuth;
import com.ruoyi.util.CommonConstant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 首页入口controller
 * FILE: com.魔金.zblog.controller.IndexController.java
 * MOTTO:  不积跬步无以至千里,不积小流无以至千里
 * AUTHOR: 魔金
 * DATE: 2017/4/8
 * TIME: 15:19
 */
@Controller
public class PageController {

    @Resource
    private ColumnListService partnerService;

    @Resource
    private ArticleListService articleService;
    @Resource
    private ColumnListService categoryService;
    @Resource
    private ColumnListService tagService;

    /**
     * 首页
     *
     * @param model
     * @return
     */
    @UnAuth
    @RequestMapping("/blog")
    public String home(Model model) {
        List<ColumnList> partnerList = partnerService.queryListOrderArticleCount();
        List<ColumnList> categoryList = categoryService.queryListOrderArticleCount();
        int articleCount = articleService.queryArticleListCount(null, -1, CommonConstant.QUERY_WITH_ISRELEASE);
        List<Map> archiveList = articleService.articleArchiveList();
        int tagCount = 5;
        model.addAttribute("categoryCount", categoryList.size());
        model.addAttribute("articleCount", articleCount);
        model.addAttribute("tagCount", tagCount);
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("partnerList", partnerList);
        model.addAttribute("archiveList", archiveList);
        return "index";
    }

    /**
     * 分类排序 暂时停用
     *
     * @return
     */
    @UnAuth
    @RequestMapping("/archives")
    @Deprecated
    public String archivesPage() {
        return "archives";
    }

    @UnAuth
    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }

    /**
     * 跳转到友链展示页面
     *
     * @return
     */
    @UnAuth
    @RequestMapping("/partner/list")
    public String partnerPage() {
        return "admin/partner/partnerList";
    }

    @UnAuth
    @RequestMapping("/about/me")
    public String aboutMe(Model model) {
        List<ColumnList> partnerList = partnerService.queryListOrderArticleCount();
        List<ColumnList> categoryList = categoryService.queryListOrderArticleCount();
        int articleCount = articleService.queryArticleListCount(null, -1, CommonConstant.QUERY_WITH_ISRELEASE);
        int tagCount = 5;
        model.addAttribute("categoryCount", categoryList.size());
        model.addAttribute("articleCount", articleCount);
        model.addAttribute("tagCount", tagCount);
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("partnerList", partnerList);
        return "aboutMe";
    }

    @UnAuth
    @RequestMapping("/popular")
    public String popularArticle(Model model) {
        List<ColumnList> partnerList = partnerService.queryListOrderArticleCount();
        List<ColumnList> categoryList = categoryService.queryListOrderArticleCount();
        int articleCount = articleService.queryArticleListCount(null, -1, CommonConstant.QUERY_WITH_ISRELEASE);
        int tagCount = 5;
        model.addAttribute("categoryCount", categoryList.size());
        model.addAttribute("articleCount", articleCount);
        model.addAttribute("tagCount", tagCount);
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("partnerList", partnerList);
        return "popular";
    }

    @UnAuth
    @RequestMapping("/thymeleaf")
    public String thymeleafPage() {
        return "thymeleaf";
    }
}
