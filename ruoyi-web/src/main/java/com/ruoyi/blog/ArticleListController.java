package com.ruoyi.blog;

import com.ruoyi.cms.bean.ArticleList;
import com.ruoyi.cms.bean.ColumnList;
import com.ruoyi.cms.service.ArticleListService;
import com.ruoyi.cms.service.ColumnListService;
import com.ruoyi.common.annotation.UnAuth;
import com.ruoyi.util.CommonConstant;
import com.ruoyi.util.PageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by GeneratorFx on 2017-04-11.
 */
@Controller
@RequestMapping("/article")
public class ArticleListController {

    @Resource
    private ColumnListService partnerService;

    @Resource
    private ArticleListService articleService;
    @Resource
    private ColumnListService categoryService;
    @Resource
    private ColumnListService tagService;

    @UnAuth
    @RequestMapping("/load")
    public String loadArticle(PageHelper<ArticleList> pageHelper, String title, Model model) {
        model.addAttribute("articleList", articleService.queryArticleList(pageHelper, title, CommonConstant.QUERY_WITH_NO_COLUMNID, -1L).getList());
        return "blog/part/articleSummary";
    }

    /**
     * 加载文章
     * 包括总标签数
     * 总文章数量
     * 分类及每个分类文章数量
     * 友链集合
     *
     * @return
     */
    @UnAuth
    @RequestMapping("/details/{articleId}")
    public String loadArticle(@PathVariable Integer articleId, Model model) {
        List<ColumnList> partnerList = partnerService.queryColumnList();
        List<ColumnList> categoryList = categoryService.queryColumnList();
        ArticleList lastArticle = articleService.getLastArticle(articleId);
        ArticleList nextArticle = articleService.getNextArticle(articleId);
        //    articleService.addArticleCount(articleId);
        int articleCount = 1;
        int tagCount = 11;
        ArticleList articleCustom = articleService.queryArticleById(articleId);
        model.addAttribute("lastArticle", lastArticle);
        model.addAttribute("nextArticle", nextArticle);
        model.addAttribute("article", articleCustom);
        model.addAttribute("categoryCount", categoryList.size());
        model.addAttribute("articleCount", articleCount);
        model.addAttribute("tagCount", tagCount);
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("partnerList", partnerList);
        return "article";
    }

    @UnAuth
    @RequestMapping("/content/search")
    public String search(PageHelper<ArticleList> pageHelper, String keyword, Model model) {
        model.addAttribute("articleList", articleService.queryArticleList(pageHelper, keyword, CommonConstant.QUERY_WITH_NO_COLUMNID, -1L).getList());

        return "blog/part/search-info";
    }


}
