/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:IndexController.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.tc;


import com.github.pagehelper.PageHelper;
import com.ruoyi.appletsutil.AppletsLoginUtils;
import com.ruoyi.cms.service.ArticleListService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.UnAuth;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.login.AppletConfig;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.tc.domain.*;
import com.ruoyi.tc.service.*;
import com.ruoyi.tc.vo.TcHomeVo;
import com.ruoyi.tc.vo.TcRoomVo;
import com.ruoyi.tc.vo.TcTcNavVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@Slf4j
@RestController
@Api(tags = "IndexController", description = "home")
public class TcIndexController extends BaseController {

    @Resource
    private ITcZhuanlangService tcZhuanlangService;
    @Resource
    private ITcGoodsService tcGoodsService;
    @Resource
    private ITcTuiguangService tcTuiguangService;
    @Resource
    private ITcQiugouService tcQiugouService;
    @Resource
    private ITcNonglinService tcNonglinService;
    @Resource
    private ITcCarService tcCarService;
    @Autowired
    private ITcRoomService tcRoomService;

    @Autowired
    private ISysDictDataService dictDataService;
    @Autowired
    private ITcJobService tcJobService;
    /**
     * 注入文章服务
     */
    @Autowired
    private ArticleListService articleListService;
    @Resource
    private ITcNavService tcNavService;
    @Autowired
    private ITcJobClassService tcJobClassService;
    @Autowired
    private ITcAdService tcAdService;
    @Autowired
    private ITcMemberService memberService;
    @Autowired
    private ITcMemberLevelService memberLevelService;
    @Autowired
    private ITcStoreInfoService tcStoreInfoService;
    @Autowired
    private ITcStroeCommentService stroeCommentService;
    @Autowired
    private ITcUserCardService userCardService;
    @Autowired
    private ITcDepositRecordService depositRecordService;
    @Autowired
    private ITcXieyiService xieyiService;

    @GetMapping("/home/level/getCateFreeNum")
    @ApiOperation(value = "首页数据", notes = "1 房屋 2 农林牧渔 3 物品交易 4推广 5 求购 6求职 7找人才 8转让 9商家店铺 10 车")
    public AjaxResult getCateFreeNum(Integer type, HttpServletRequest request) {
        TcDepositRecord record = new TcDepositRecord();
        Long userId = AppletsLoginUtils.getInstance().getCustomerId(request);
        record.setCustomerId(userId);
        record.setStatus("2");
        Long count = 0L; // 用户当月已发布数量
        if (type == 1) {
            count = tcRoomService.countByMonth(userId);
        } else if (type == 2) {
            count = tcNonglinService.countByMonth(userId);
        } else if (type == 3) {
            count = tcGoodsService.countByMonth(userId);
        } else if (type == 4) {
            count = tcTuiguangService.countByMonth(userId);
        } else if (type == 5) {
            count = tcQiugouService.countByMonth(userId);
        } else if (type == 6) {
            count = tcJobService.countByMonth(userId);
        } else if (type == 8) {
            count = tcZhuanlangService.countByMonth(userId);
        } else if (type == 10) {
            count = tcCarService.countByMonth(userId);
        }
        List<TcDepositRecord> list = depositRecordService.selectTcDepositRecordList(record);
        TcDepositRecord depositRecord = list.stream().filter(ruleInfo -> ruleInfo.getCloseTime().after(new Date())).findFirst().orElse(null);
        if (depositRecord != null) {
            TcMemberLevel level = memberLevelService.selectTcMemberLevelById(depositRecord.getMemberLevelId());
            level.setPublishedCount(count);
            level.setCloseTime(depositRecord.getCloseTime());
            return AjaxResult.success(level);
        }
        TcMemberLevel level = memberLevelService.selectTcMemberLevelById(AppletConfig.defaultMemberLevel);
        level.setPublishedCount(count);
        return AjaxResult.success(level);
    }

    @UnAuth
    @GetMapping("/home/getLabel")
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public AjaxResult getLabel(SysDictData tagParam) {
        tagParam.setStatus("0");
        List<SysDictData> tcTagList = dictDataService.selectDictDataList(tagParam);
        return AjaxResult.success(tcTagList);
    }

    @UnAuth
    @GetMapping("/home/init")
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public AjaxResult index() {
        TcAd adParam = new TcAd();
        List<TcAd> tcAdList = tcAdService.selectTcAdList(adParam);
        List<TcAd> car_banner = new ArrayList<>();
        List<TcAd> find_banner = new ArrayList<>();
        List<TcAd> home_banner = new ArrayList<>();
        TcAd home_ad = new TcAd();
        for (TcAd tag : tcAdList) {
            if (tag.getType().equals("car_banner")) {
                car_banner.add(tag);
            } else if (tag.getType().equals("find_banner")) {
                find_banner.add(tag);
            } else if (tag.getType().equals("home_ad")) {
                home_ad = tag;
            } else if (tag.getType().equals("home_banner")) {
                home_banner.add(tag);
            }
        }
        TcNav homeQueryNav = new TcNav();
        homeQueryNav.setStatus("normal");
        homeQueryNav.setPid(0L);
        List<TcNav> tcNavList = tcNavService.selectTcNavList(homeQueryNav);
        List<TcNav> home_menu = new ArrayList<>();
        List<TcNav> find_menu = new ArrayList<>();
        for (TcNav tag : tcNavList) {
            if (tag.getType().equals("home")) {
                home_menu.add(tag);
            } else if (tag.getType().equals("find")) {
                find_menu.add(tag);
            }
        }
        TcHomeVo indexVo = TcHomeVo.builder()
                .home_menu(home_menu)
                .find_menu(find_menu)
                .app_name(AppletConfig.TITLE)
                .car_banner(car_banner)
                .home_banner(home_banner)
                .find_banner(find_banner)
                .home_ad(home_ad)
                .build();
        return AjaxResult.success(indexVo);
    }

    @UnAuth
    @GetMapping("/home/job/getNav") // /addons/魔金同城/job/getJobCategory
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public AjaxResult getNav(TcNav tagParam) {
        List<TcTcNavVo> tcTagList = tcNavService.queryAllFirstAndSecondCategory(tagParam);
        return AjaxResult.success(tcTagList);
    }

    @UnAuth
    @GetMapping("/home/job/getJobCategory") // /addons/魔金同城/job/getJobCategory
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public AjaxResult getJobCategory(TcJobClass tagParam) {
        List<TcJobClass> tcTagList = tcJobClassService.queryAllFirstAndSecondCategory(tagParam);
        return AjaxResult.success(tcTagList);
    }

    @UnAuth
    @GetMapping("/home/job/list")///addons/魔金同城/job/list
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public TableDataInfo jobList(TcJob tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcJob> tcTagList = tcJobService.selectTcJobList(tagParam);
        return getDataTables(tcTagList);
    }

    /**
     * 房屋详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/job/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "房屋详情", businessType = BusinessType.SELECT)
    public AjaxResult jobDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            TcJob job = tcJobService.selectTcJobById(id);
            if (job.getJobClassId() != null) {
                TcJobClass jobcategory = tcJobClassService.selectTcJobClassById(Long.parseLong(job.getJobClassId()));
                if (jobcategory != null && jobcategory.getId() > 0) {
                    job.setJobcategory(jobcategory);
                }
            }
            return AjaxResult.success(job);
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/room/index")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public AjaxResult roomIndex(TcRoom tagParam) {
        tagParam.setStatus("1");
        List<TcRoom> tcTagList = tcRoomService.selectTcRoomList(tagParam);
        List<TcRoom> rent_list = new ArrayList<>();
        List<TcRoom> sell_list = new ArrayList<>();
        List<TcRoom> storeInfoList = new ArrayList<>();
        for (TcRoom tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserType().equals("出售")) {
                sell_list.add(tcTuiguang);
            }
            if (tcTuiguang.getUserType().equals("出租")) {
                rent_list.add(tcTuiguang);
            }
            if (tcTuiguang.getRootType().equals("商铺")) {
                storeInfoList.add(tcTuiguang);
            }
        }
        TcAd adParam = new TcAd();
        adParam.setType("find_banner");
        List<TcAd> find_menu = tcAdService.selectTcAdList(adParam);
        TcStoreInfo storeInfoParam = new TcStoreInfo();
        storeInfoParam.setStatus("1");

        TcRoomVo indexVo = TcRoomVo.builder()
                .shop_list(storeInfoList)
                .banner(find_menu)
                .rent_list(rent_list)
                .sell_list(sell_list)
                .build();
        return AjaxResult.success(indexVo);
    }

    @UnAuth
    @GetMapping("/home/store/list")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public TableDataInfo storeList(TcStoreInfo tagParam) {
        tagParam.setStatus("1");
        List<TcStoreInfo> tcTagList = new ArrayList<>();
        if (StringUtils.isNotEmpty(tagParam.getLin()) && Integer.parseInt(tagParam.getLin()) > 0) {
            tcTagList = tcStoreInfoService.selectTcStoreInfoListByDistinct(tagParam);
        } else {
            tcTagList = tcStoreInfoService.selectTcStoreInfoList(tagParam);
        }
        for (TcStoreInfo tcUserCard : tcTagList) {
            if (tcUserCard.getClassId() != null) {
                TcNav jobcategory = tcNavService.selectTcNavById(tcUserCard.getClassId());
                if (jobcategory != null && jobcategory.getId() > 0) {
                    tcUserCard.setCategory(jobcategory);
                    TcNav pcategory = tcNavService.selectTcNavById(jobcategory.getPid());
                    if (pcategory != null && pcategory.getId() > 0) {
                        tcUserCard.setPCategory(pcategory);
                    }
                }
            }
        }
        return getDataTables(tcTagList);
    }

    @UnAuth
    @GetMapping("/home/room/list")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public TableDataInfo roomList(TcRoom tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcRoom> tcTagList = tcRoomService.selectTcRoomList(tagParam);
        for (TcRoom tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 房屋详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/room/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "房屋详情", businessType = BusinessType.SELECT)
    public AjaxResult roomDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcRoomService.selectTcRoomById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/car/list")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public TableDataInfo carList(TcCar tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcCar> tcTagList = tcCarService.selectTcCarList(tagParam);
        for (TcCar tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 房屋详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/car/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "房屋详情", businessType = BusinessType.SELECT)
    public AjaxResult carDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcCarService.selectTcCarById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/tuiguang/list")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "tuig", notes = "推广")
    public TableDataInfo tuiguangList(TcTuiguang tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcTuiguang> tcTagList = tcTuiguangService.selectTcTuiguangList(tagParam);
        for (TcTuiguang tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 推广详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/tuiguang/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "推广详情", businessType = BusinessType.SELECT)
    public AjaxResult tuiguangDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcTuiguangService.selectTcTuiguangById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/qiugou/list")// /home/qiugou/list
    @ApiOperation(value = "求购", notes = "求购")
    public TableDataInfo qiugouList(TcQiugou tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcQiugou> tcTagList = tcQiugouService.selectTcQiugouList(tagParam);
        for (TcQiugou tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }

        return getDataTables(tcTagList);
    }

    /**
     * 求购详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/qiugou/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "求购详情", businessType = BusinessType.SELECT)
    public AjaxResult qiugouDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcQiugouService.selectTcQiugouById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/goods/list")// /home/qiugou/list
    @ApiOperation(value = "", notes = "物品交易")
    public TableDataInfo goodsList(TcGoods tagParam, HttpServletRequest request) {
        tagParam.setStatus("1");
        startPage();
        List<TcGoods> tcTagList = tcGoodsService.selectTcGoodsList(tagParam);
        for (TcGoods tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 物品交易详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/goods/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "物品交易详情", businessType = BusinessType.SELECT)
    public AjaxResult goodsDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcGoodsService.selectTcGoodsById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/nonglin/list")// /home/qiugou/list
    @ApiOperation(value = "", notes = "农林牧渔")
    public TableDataInfo nonglinList(TcNonglin tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcNonglin> tcTagList = tcNonglinService.selectTcNonglinList(tagParam);
        for (TcNonglin tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 农林牧渔详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/nonglin/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "农林牧渔详情", businessType = BusinessType.SELECT)
    public AjaxResult nonglinDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcNonglinService.selectTcNonglinById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/user/list")// /home/qiugou/list
    @ApiOperation(value = "", notes = "找人才")
    public TableDataInfo userCardList(TcUserCard tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcUserCard> tcTagList = userCardService.selectTcUserCardList(tagParam);
        for (TcUserCard tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
            if (tcTuiguang.getJobClassId() != null) {
                TcJobClass jobcategory = tcJobClassService.selectTcJobClassById(Long.parseLong(tcTuiguang.getJobClassId()));
                if (jobcategory != null && jobcategory.getId() > 0) {
                    tcTuiguang.setJobcategory(jobcategory);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 找人才详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/user/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "找人才详情", businessType = BusinessType.SELECT)
    public AjaxResult userCardDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(userCardService.selectTcUserCardById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/zhuanrang/list")// /home/qiugou/list
    @ApiOperation(value = "", notes = "生意转让")
    public TableDataInfo zhuanrangList(TcZhuanlang tagParam) {
        tagParam.setStatus("1");
        startPage();
        List<TcZhuanlang> tcTagList = tcZhuanlangService.selectTcZhuanlangList(tagParam);
        for (TcZhuanlang tcTuiguang : tcTagList) {
            if (tcTuiguang.getUserId() != null) {
                TcMember member = memberService.selectTcMemberById(tcTuiguang.getUserId());
                if (member != null && member.getId() > 0) {
                    tcTuiguang.setMember(member);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    /**
     * 生意转让详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/zhuanrang/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "生意转让详情", businessType = BusinessType.SELECT)
    public AjaxResult zhuanrangDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(tcZhuanlangService.selectTcZhuanlangById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }

    @UnAuth
    @GetMapping("/home/xieyi/list")// /home/qiugou/list
    @ApiOperation(value = "", notes = "生意转让")
    public TableDataInfo xieyiList(TcXieyi tagParam) {
        startPage();
        List<TcXieyi> tcTagList = xieyiService.selectTcXieyiList(tagParam);
        return getDataTables(tcTagList);
    }

    /**
     * 生意转让详情
     *
     * @return 返回分类集合
     */
    @GetMapping("/home/xieyi/detail")// /addons/魔金同城/house/main
    @UnAuth
    @Log(title = "生意转让详情", businessType = BusinessType.SELECT)
    public AjaxResult xieyiDetail(@RequestParam(value = "id", required = true) long id) {
        try {
            return AjaxResult.success(xieyiService.selectTcXieyiById(id));
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
    }
    @GetMapping("/home/store/detail")// /addons/魔金同城/house/main
    @Log(title = "商铺详情", businessType = BusinessType.SELECT)
    public AjaxResult userStore(@RequestParam(value = "id", required = true) long id) {
        try {
                TcStoreInfo tcUserCard = tcStoreInfoService.selectTcStoreInfoById(id);
                if (tcUserCard.getClassId() != null) {
                    TcNav jobcategory = tcNavService.selectTcNavById(tcUserCard.getClassId());
                    if (jobcategory != null && jobcategory.getId() > 0) {
                        tcUserCard.setCategory(jobcategory);
                        TcNav pcategory = tcNavService.selectTcNavById(jobcategory.getPid());
                        if (pcategory != null && pcategory.getId() > 0) {
                            tcUserCard.setPCategory(pcategory);
                        }
                    }
                }
                tcUserCard.setHit(tcUserCard.getHit() + 1);
                tcStoreInfoService.updateTcStoreInfo(tcUserCard);
                TcStroeComment tagParam = new TcStroeComment();
                tagParam.setStoreId(tcUserCard.getId());
                PageHelper.startPage(1, 5);
                List<TcStroeComment> stroeCommentList = stroeCommentService.selectTcStroeCommentList(tagParam);
                tcUserCard.setStroeCommentList(stroeCommentList);
                return AjaxResult.success(tcUserCard);

        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }

    }
    @UnAuth
    @GetMapping("/home/memberLevel/list")// /home/qiugou/list
    @ApiOperation(value = "", notes = "会员等级")
    public AjaxResult memberLevelList(TcMemberLevel tagParam) {
        List<TcMemberLevel> tcTagList = memberLevelService.selectTcMemberLevelList(tagParam);
        return AjaxResult.success(tcTagList);
    }

}
