/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:IndexController.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.tc;


import com.ruoyi.appletsutil.AppletsLoginUtils;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.tc.domain.*;
import com.ruoyi.tc.service.*;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@Slf4j
@RestController
@Api(tags = "MyTcPublishController", description = "home")
public class MyTcUpdateController extends BaseController {

    @Resource
    private ITcZhuanlangService tcZhuanlangService;
    @Resource
    private ITcGoodsService tcGoodsService;
    @Resource
    private ITcTuiguangService tcTuiguangService;
    @Resource
    private ITcQiugouService tcQiugouService;
    @Resource
    private ITcNonglinService tcNonglinService;
    @Resource
    private ITcCarService tcCarService;
    @Autowired
    private ITcRoomService tcRoomService;

    @Autowired
    private ISysDictDataService dictDataService;
    @Autowired
    private ITcJobService tcJobService;

    @Resource
    private ITcNavService tcNavService;
    @Autowired
    private ITcJobClassService tcJobClassService;
    @Autowired
    private ITcAdService tcAdService;
    @Autowired
    private ITcMemberService memberService;
    @Autowired
    private ITcMemberLevelService memberLevelService;
    @Autowired
    private ITcStoreInfoService tcStoreInfoService;
    @Autowired
    private ITcUserCardService userCardService;
    @Autowired
    private ITcDepositRecordService depositRecordService;

    /**
     * 修改车况信息
     */
    @Log(title = "修改车况信息", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcCar")
    public AjaxResult updateTcCar(@RequestBody TcCar tcCar) {
        return toAjax(tcCarService.updateTcCar(tcCar));
    }

    /**
     * 修改物品交易
     */
    @Log(title = "修改物品交易", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcGoods")
    public AjaxResult updateTcGoods(@RequestBody TcGoods tcCar) {
        return toAjax(tcGoodsService.updateTcGoods(tcCar));
    }

    /**
     * 修改推广信息
     */
    @Log(title = "修改推广信息", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcTuiguang")
    public AjaxResult updateTcTuiguang(@RequestBody TcTuiguang tcCar) {
        return toAjax(tcTuiguangService.updateTcTuiguang(tcCar));
    }

    /**
     * 修改转让信息
     */
    @Log(title = "修改转让信息", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcZhuanlang")
    public AjaxResult updateTcZhuanlang(@RequestBody TcZhuanlang tcCar) {
        return toAjax(tcZhuanlangService.updateTcZhuanlang(tcCar));
    }

    /**
     * 修改房产信息
     */
    @Log(title = "修改房产信息", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcRoom")
    public AjaxResult updateTcRoom(@RequestBody TcRoom tcCar) {
        return toAjax(tcRoomService.updateTcRoom(tcCar));
    }

    /**
     * 修改招聘信息
     */
    @Log(title = "修改招聘信息", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcJob")
    public AjaxResult updateTcJob(@RequestBody TcJob tcCar) {
        return toAjax(tcJobService.updateTcJob(tcCar));
    }

    /**
     * 修改农林牧渔
     */
    @Log(title = "修改农林牧渔", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcNonglin")
    public AjaxResult updateTcNonglin(@RequestBody TcNonglin tcCar) {
        return toAjax(tcNonglinService.updateTcNonglin(tcCar));
    }

    /**
     * 修改个人信息
     */
    @Log(title = "修改个人信息", businessType = BusinessType.UPDATE)
    @PostMapping("tc/updateTcMember")
    public AjaxResult updateTcMember(@RequestBody TcMember tcCar, HttpServletRequest request) {
        tcCar.setId(AppletsLoginUtils.getInstance().getCustomerId(request));
        memberService.updateTcMember(tcCar);
        return AjaxResult.success(tcCar);
    }

}
