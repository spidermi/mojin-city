/*
 * Copyright (c) 2021
 * User:魔金多商户商城
 * File:IndexController.java
 * Date:2020/09/13 08:43:13
 */

package com.ruoyi.tc;


import com.github.pagehelper.PageHelper;
import com.ruoyi.appletsutil.AppletsLoginUtils;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.tc.domain.*;
import com.ruoyi.tc.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@Slf4j
@RestController
@Api(tags = "MyTcPublishController", description = "home")
public class MyTcPublishController extends BaseController {

    @Resource
    private ITcZhuanlangService tcZhuanlangService;
    @Resource
    private ITcGoodsService tcGoodsService;
    @Resource
    private ITcTuiguangService tcTuiguangService;
    @Resource
    private ITcQiugouService tcQiugouService;
    @Resource
    private ITcNonglinService tcNonglinService;
    @Resource
    private ITcCarService tcCarService;
    @Autowired
    private ITcRoomService tcRoomService;

    @Autowired
    private ISysDictDataService dictDataService;
    @Autowired
    private ITcJobService tcJobService;

    @Resource
    private ITcNavService tcNavService;
    @Autowired
    private ITcJobClassService tcJobClassService;
    @Autowired
    private ITcAdService tcAdService;
    @Autowired
    private ITcMemberService memberService;
    @Autowired
    private ITcMemberLevelService memberLevelService;
    @Autowired
    private ITcStoreInfoService tcStoreInfoService;
    @Autowired
    private ITcUserCardService userCardService;
    @Autowired
    private ITcDepositRecordService depositRecordService;
    @Autowired
    private ITcStroeCommentService stroeCommentService;

    @GetMapping("/my/job/list")///addons/魔金同城/job/list
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public TableDataInfo jobList(TcJob tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcJob> tcTagList = tcJobService.selectTcJobList(tagParam);
        return getDataTables(tcTagList);

    }


    @GetMapping("/my/room/list")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "首页数据", notes = "首页数据")
    public TableDataInfo roomList(TcRoom tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcRoom> tcTagList = tcRoomService.selectTcRoomList(tagParam);
        return getDataTables(tcTagList);
    }

    @GetMapping("/my/tuiguang/list")// /addons/魔金同城/house/list?info_type=rent
    @ApiOperation(value = "tuig", notes = "推广")
    public TableDataInfo tuiguangList(TcTuiguang tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcTuiguang> tcTagList = tcTuiguangService.selectTcTuiguangList(tagParam);
        return getDataTables(tcTagList);
    }

    @GetMapping("/my/qiugou/list")// /my/qiugou/list
    @ApiOperation(value = "求购", notes = "求购")
    public TableDataInfo qiugouList(TcQiugou tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcQiugou> list = tcQiugouService.selectTcQiugouList(tagParam);
        return getDataTables(list);
    }

    @GetMapping("/my/car/list")// /my/qiugou/list
    @ApiOperation(value = "求购", notes = "求购")
    public TableDataInfo qiugouList(TcCar tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcCar> list = tcCarService.selectTcCarList(tagParam);
        return getDataTables(list);
    }

    @GetMapping("/my/goods/list")// /my/qiugou/list
    @ApiOperation(value = "", notes = "物品交易")
    public TableDataInfo goodsList(TcGoods tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcGoods> list = tcGoodsService.selectTcGoodsList(tagParam);
        return getDataTables(list);
    }

    @GetMapping("/my/nonglin/list")// /my/qiugou/list
    @ApiOperation(value = "", notes = "农林牧渔")
    public TableDataInfo nonglinList(TcNonglin tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcNonglin> list = tcNonglinService.selectTcNonglinList(tagParam);
        return getDataTables(list);
    }

    @GetMapping("/my/user/list")// /my/qiugou/list
    @ApiOperation(value = "", notes = "找人才")
    public TableDataInfo userCardList(TcUserCard tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcUserCard> tcTagList = userCardService.selectTcUserCardList(tagParam);
        for (TcUserCard tcTuiguang : tcTagList) {
            if (tcTuiguang.getJobClassId() != null) {
                TcJobClass jobcategory = tcJobClassService.selectTcJobClassById(Long.parseLong(tcTuiguang.getJobClassId()));
                if (jobcategory != null && jobcategory.getId() > 0) {
                    tcTuiguang.setJobcategory(jobcategory);
                }
            }
        }
        return getDataTables(tcTagList);
    }

    @GetMapping("/my/zhuanrang/list")// /my/qiugou/list
    @ApiOperation(value = "", notes = "生意转让")
    public TableDataInfo zhuanrangList(TcZhuanlang tagParam, HttpServletRequest request) {
        tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcZhuanlang> list = tcZhuanlangService.selectTcZhuanlangList(tagParam);
        return getDataTables(list);
    }

    @GetMapping("/my/userCard")// /addons/魔金同城/house/main
    @Log(title = "找人才详情", businessType = BusinessType.SELECT)
    public AjaxResult userCardDetail(HttpServletRequest request) {
        try {
            TcUserCard userCard = new TcUserCard();
            userCard.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
            List<TcUserCard> userCards = userCardService.selectTcUserCardList(userCard);
            if (userCards != null && userCards.size() > 0) {
                TcUserCard tcUserCard = userCards.get(0);
                if (tcUserCard.getJobClassId() != null) {
                    TcJobClass jobcategory = tcJobClassService.selectTcJobClassById(Long.parseLong(tcUserCard.getJobClassId()));
                    if (jobcategory != null && jobcategory.getId() > 0) {
                        tcUserCard.setJobcategory(jobcategory);
                    }
                }
                return AjaxResult.success(tcUserCard);
            }
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success();
    }

    @GetMapping("/my/userStore")// /addons/魔金同城/house/main
    @Log(title = "我的商铺", businessType = BusinessType.SELECT)
    public AjaxResult userStore(HttpServletRequest request) {
        try {
            TcStoreInfo userCard = new TcStoreInfo();
            userCard.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
            List<TcStoreInfo> userCards = tcStoreInfoService.selectTcStoreInfoList(userCard);
            if (userCards != null && userCards.size() > 0) {
                TcStoreInfo tcUserCard = userCards.get(0);
                if (tcUserCard.getClassId() != null) {
                    TcNav jobcategory = tcNavService.selectTcNavById(tcUserCard.getClassId());
                    if (jobcategory != null && jobcategory.getId() > 0) {
                        tcUserCard.setCategory(jobcategory);
                        TcNav pcategory = tcNavService.selectTcNavById(jobcategory.getPid());
                        if (pcategory != null && pcategory.getId() > 0) {
                            tcUserCard.setPCategory(pcategory);
                        }
                    }
                }
                tcUserCard.setHit(tcUserCard.getHit() + 1);
                tcStoreInfoService.updateTcStoreInfo(tcUserCard);
                TcStroeComment tagParam = new TcStroeComment();
                tagParam.setStoreId(tcUserCard.getId());
                PageHelper.startPage(1, 5);
                List<TcStroeComment> stroeCommentList = stroeCommentService.selectTcStroeCommentList(tagParam);
                tcUserCard.setStroeCommentList(stroeCommentList);
                return AjaxResult.success(tcUserCard);
            }
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success();
    }

    @GetMapping("/my/stroeComment/list")// /my/qiugou/list
    @ApiOperation(value = "", notes = "商铺点评")
    public TableDataInfo stroeCommentList(TcStroeComment tagParam, HttpServletRequest request) {
       // tagParam.setUserId(AppletsLoginUtils.getInstance().getCustomerId(request));
        startPage();
        List<TcStroeComment> list = stroeCommentService.selectTcStroeCommentList(tagParam);
        return getDataTables(list);
    }

    @GetMapping("/my/userInfo")// /addons/魔金同城/house/main
    @Log(title = "找人才详情", businessType = BusinessType.SELECT)
    public AjaxResult userInfo(HttpServletRequest request) {
        return AjaxResult.success(memberService.selectTcMemberById(AppletsLoginUtils.getInstance().getCustomerId(request)));
    }

    @GetMapping("/my/getUserVip")// /addons/魔金同城/house/main
    @Log(title = "找人才详情", businessType = BusinessType.SELECT)
    public AjaxResult getUserVip(HttpServletRequest request) {
        return AjaxResult.success(memberService.selectTcMemberById(AppletsLoginUtils.getInstance().getCustomerId(request)));
    }
}
