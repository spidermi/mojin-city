const ENV_BASE_URL = {
	development: 'http://localhost:8091', //开发环境 http://localhost:8091
	production: 'https://mojin.51wangshi.com/api-web', //生产环境
}
export const API_URL = ENV_BASE_URL[process.env.NODE_ENV || 'development'];
export const APP_NAME = '魔金同城'; //应用名
export const APP_MOBILE = '13146587722'; //客服电话