export default {
	state: {
		isLogin: false,
		userInfo: null,
	},
	getters: {

	},
	mutations: {
		setUserInfo(state, data) {
			state.userInfo = data
		},
		setLogin(state, data) {
			state.isLogin = data
		},
	},
	actions: {
		loginOut({commit}) {
			console.log('loginOut')
			commit('setLogin', false);
			commit('setUserInfo', null);
			uni.removeStorageSync('token');
			uni.removeStorageSync('userInfo');
		},
		login({commit}, userInfo) {

			commit('setLogin', true);
			commit('setUserInfo', userInfo.member);
			uni.setStorageSync('token', userInfo.access_token);

			uni.setStorageSync('userInfo', userInfo.member);
		},
		upUserInfo({
			commit
		}, userInfo) {
			commit('setUserInfo', userInfo);
			uni.setStorageSync('userInfo', userInfo);
		}
	}
}
